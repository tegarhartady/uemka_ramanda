<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function emailCheck()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $email = $request['email'];

        $users = DB::select("SELECT * FROM apiuemka.users WHERE email = '$email'");

        foreach ($users as $u) {
            $user = $u;
        }

        if (!empty($user)) {
            try {
                $user = User::whereEmail($email)->first();
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                // if ($request->remember_me)
                //     $token->expires_at = Carbon::now()->addWeeks(1);
                $token->save();

                $data = [
                    'ResponseCode' => '00',
                    'Users' => $user,
                    'access_token' => $tokenResult->accessToken,
                    'ResponseDescription' => 'Email ditemukan'
                ];
            } catch (\Throwable $e) {
                return $e;
            }
        } else {
            $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Email tidak ditemukan'
            ];
        }

        // Return response
        return response()->json($data, 200);
    }
}
