<?php

namespace App\Http\Controllers;

use App\Artikel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArtikelController extends Controller
{

    function slugify($text)
    {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
    }
    public function index()
    {
        return response()->json(Artikel::all(),200);
    }
    public function whereslug()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        return response()->json(Artikel::all()->where('slug','=',$request['slug'])->first(),200);
        // return $this->slugify("Promo Semangat Akhir Tahun Dana Cair Express dengan BPKB Mobil");
        
    }
    
    public function tambah_artikel(Request $request)
    {
        // dd("test");

        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        try {
            // $request->validate([
            //     'judul' => 'required|unique:article',
            //     'foto' => 'required',
            //     'diupload' => 'required',
            //     'isi' => 'required',
            //     // 'publish_at' => 'nullable|date',
            // ]);

            $artikel = new Artikel;
            $artikel->judul_article = $request['judul'];
            $artikel->slug = $this->slugify($request['judul']);
            $photo    = str_replace('data:image/png;base64,', '', $request['foto_article']);
            $photoktp = time() . '_' . uniqid() . '.' . 'png';
            $defaultfile = '/' . env('INPUT_PHOTO', '') . '/' . 'Artikel';
            Storage::disk('sftp')->put($defaultfile . $photoktp, base64_decode($photo));
            $artikel->foto_article = $photoktp;
            $artikel->by_upload = $request['diupload'];
            $artikel->isi_article = $request['isi'];
            $artikel->updated_at = Carbon::now();
            $artikel->save();
                return response()->json([
                    'message' => 'Artikel Berhasil Di Tambahkan!'
                ], 201);
        } catch (\Throwable $th) {
            //throw $th;
            return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Article Gagal Di Tambahkan',
                'message' => $th->getMessage(),
            ];
        }
    }

    public function update_artikel(Request $request, $id)
    {
        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        $check_artikel = Artikel::firstWhere('idarticle',$id);
        if (!empty($check_artikel)) {
            try {
                $artikel = Artikel::find($id);
                $artikel->judul_article = $request['judul'];
                $artikel->foto_article = $request['foto'];
                $artikel->by_upload = $request['diupload'];
                $artikel->isi_article = $request['isi'];
                $artikel->updated_at = Carbon::now();
                $artikel->save();
                return response()->json([
                    'message' => 'Artikel Berhasil Di Update!'
                ], 201);
            } catch (\Throwable $th) {
                //throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'Article Gagal Di Update',
                    'message' => $th->getMessage(),
                ];
            }
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Artikel Tidak Ditemukan'
            ], 404);
        }
    }

    public function destroy($id)
    {
        $checkid = Artikel::firstWhere('idarticle',$id);
        if ($checkid) {
            Artikel::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Artikel Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Artikel Tidak Di Temukan'
            ],401);
        }
    }

    public function fullarticle(Request $request)
    {
        // dd("test");
        return response()->json(Artikel::all()->where('idarticle','=',$request['idarticle']));
    }
}
