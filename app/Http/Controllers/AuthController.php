<?php

namespace App\Http\Controllers;

use App\Partner;
use App\User;
use App\UsersLog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function signup()
    {

        $json = file_get_contents('php://input');

        // merubah json ke string
        $request = json_decode($json, true);

        // Where Data Email
        $emails = User::all()->where('email', '=', $request['email']);

        foreach ($emails as $a) {
            $email = $a->email;
        }

        if (!empty($email)) {
            return $data=[
                'ResponseCode' => '99',
                'ResponseDirection' => 'Email Sudah Terdaftar'
            ];
        } else {
            try {
                // $request->validate([
                //     'name' => 'required|string',
                //     'email' => 'required|string|email|unique:users',
                //     'sosmed' => 'required|string',
                //     // 'no_telp' => 'required|string',
                //     'password' => 'required|string|confirmed'                
                // ]);
                $user = new User();
                $user->name = $request['name'];
                $user->email = $request['email'];
                // $user->sosmed = $request['sosmed'];
                $user->no_telp = $request['no_telp'];
                // 'password' = bcrypt($request->password);
                $user->password = Hash::make($request['password']);
                $user->level = '2';
                    // 'token_api' => Str::random(16)
                
                // dd($user);
                // $log = new UsersLog();
                // $log->log_name = $user->name;
                // // $log->log_idlogin = $request->id;
                // $log->log_tanggal = Carbon::now()->format('Y-m-d');
                // $log->log_waktu = Carbon::now()->format("H:i:s");
                // $log->log_aktivitas = "Register";
                // dd($log);
                // $log->save();
                $user->save();

                return response()->json([
                    'message' => 'User Berhasil Terdaftar!'
                ], 201);
            } catch (\Throwable $th) {
                // throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'User Gagal Terdaftar',
                    'message' => $th->getMessage(),
                ];
            }
            
        }
    }

    public function login(Request $request)
    {
        
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $email = $request['email'];
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        // $logging = DB::select("SELECT `name` FROM users WHERE email = '$email'");
        // $loggingid = DB::select("SELECT id FROM users WHERE email = '$email'");

        $log = new UsersLog();
        $log->log_name = $request->user()->name;
        $log->log_idlogin = $request->user()->id;
        $log->log_tanggal = Carbon::now()->format('Y-m-d');
        $log->log_waktu = Carbon::now()->format("H:i:s");
        $log->log_aktivitas = "Login";
        // dd($log);
        $log->save();

        // login sebagai lembaga
        
        return response()->json([
            'message' => 'Anda Berhasil Login',
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString(),
            'id_login' => $request->user()->id,
            'nama_lengkap' => $request->user()->name,
            'email' => $request->user()->email
        ]);

        $users =  User::all()->where('email', '=', $request['email']);

        foreach ($users as $u) {
            $user = $u;
            $photo =  env('APP_URL2', '') . '/storage/' . env('INPUT_PHOTO', '') . '/Profile/' . $u->photoURL;
        }
    }

    public function logout(Request $request)
    {
        // dd("test");
        $log = new UsersLog();
        $log->log_name = $request->user()->name;
        $log->log_idlogin = $request->user()->id;
        $log->log_tanggal = Carbon::now()->format('Y-m-d');
        $log->log_waktu = Carbon::now()->format("H:i:s");
        $log->log_aktivitas = "Logout";
        // dd($log);
        $log->save();
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Berhasil Logout'
        ]);
    }

    public function user(Request $request)
    {
        if (!empty($request)) {
            return response()->json($request->user());
        } else {
            return response()->json([
                'Message' => 'Harap Login terlebih dahulu'
            ],401);
        }
    }

    public function view_user()
    {
        return response()->json(
            User::all(),200
        );
    }

    public function sendPasswordResetToken(Request $request)
    {
        // $json = file_get_contents('php://input');
        // merubah json ke string
        // $request = json_decode($json, true);
        
        $user = User::where('email', $request->email)->first();
        if ( !$user ) return redirect()->back()->withErrors(['error' => '404']);

        //create a new token to be sent to the user. 
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60), //change 60 to any length you want
            'created_at' => Carbon::now()
        ]);

        $tokenData = DB::table('password_resets')
        ->where('email', $request->email)->first();

        $token = $tokenData->token;
        $email = $request->email; // or $email = $tokenData->email;
        $log = new UsersLog();
        $log->log_name = $request->user()->name;
        $log->log_idlogin = $request->user()->id;
        $log->log_tanggal = Carbon::now()->format('Y-m-d');
        $log->log_waktu = Carbon::now()->format("H:i:s");
        $log->log_aktivitas = "Send Email for Reset Password ";
        // dd($log);
        $log->save();
        // dd($token,$email);
        if ($token || $email) {
            return response()->json([
                'message' => 'Berhasil mengirim'
            ],200);
        } else {
            return response()->json([
                'message' => 'tidak berhasil mengirim'
            ],301);
        }
    /**
        * Send email to the email above with a link to your password reset
        * something like url('password-reset/' . $token)
        * Sending email varies according to your Laravel version. Very easy to implement
        */
    }

    public function resetPassword(Request $request, $token)
    {
        //some validation
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);
   
        $password = $request->password;
        $tokenData = DB::table('password_resets')
        ->where('token', $token)->first();
   
        $user = User::where('email', $tokenData->email)->first();
        if ( !$user ) return redirect()->to('home'); //or wherever you want
   
        $user->password = Hash::make($password);
        $user->update(); //or $user->save();

        $log = new UsersLog();
        $log->log_name = $request->user()->name;
        $log->log_idlogin = $request->user()->id;
        $log->log_tanggal = Carbon::now()->format('Y-m-d');
        $log->log_waktu = Carbon::now()->format("H:i:s");
        $log->log_aktivitas = "Ganti Password";
        // dd($log);
        $log->save();
   
        //do we log the user directly or let them login and try their password for the first time ? if yes 
        Auth::login($user);
   
       // If the user shouldn't reuse the token later, delete the token 
       DB::table('password_resets')->where('email', $user->email)->delete();
   
       //redirect where we want according to whether they are logged in or not.
    }
    public function changePassword()
    {
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        if ($request) {

            // get request
            $id = $request['userid'];
            $oldpassword = $request['oldpassword'];
            $newpassword = $request['newpassword'];
            $confirmpassword = $request['confirmpassword'];

            
            // Cek LoginID in Database
            $cekLoginID = DB::select("SELECT * FROM uemka.users WHERE id = '$id'");

            // Jika Cek Login tidak kosong
            if (!empty($cekLoginID)) {
                foreach ($cekLoginID as $old) {
                    if (password_verify($oldpassword, $old->password)) {
                        // Jika Password Sama
                        if ($newpassword === $confirmpassword) {
                            // Action Hash Password
                            $hashnewpassword = Hash::make($newpassword);
                            try {
                                // Update Password yang terhash
                                DB::update("UPDATE `uemka`.`users` SET `password` = '$hashnewpassword' WHERE (`id` = '$id');");
                                // DB::connection('mysql3')->update("")
                                // Return hasil
                                $log = new UsersLog();
                                // $log->log_name = $request->user()->name;
                                $log->log_idlogin = $request['userid'];
                                $log->log_tanggal = Carbon::now()->format('Y-m-d');
                                $log->log_waktu = Carbon::now()->format("H:i:s");
                                $log->log_aktivitas = "Ganti Password";
                                // dd($log);
                                $log->save();
                                return $data = [
                                    'ResponseCode' => '00',
                                    'ResponseDescription' => 'Berhasil Mengubah Password'
                                ];
                            } catch (\Throwable $th) {
                                // Return Hasil
                                return $data = [
                                    'ResponseCode' => '99',
                                    'ResponseDescription' => 'Gagal Mengubah Password'
                                ];
                            }
                        } else {
                            // Return Hasil
                            $data = [
                                'ResponseCode' => '99',
                                'ResponseDescription' => 'Password Not Same'
                            ];
                        }
                    } else {
                        $data = [
                            'ResponseCode' => '99',
                            'ResponseDescription' => 'Password old wrong'
                        ];
                    }
                }
            } else {
                $data = [
                    'ResponseCode' => 99,
                    'ResponseDescription' => 'Data tidak ditemukan'
                ];
            }
        }
        // Return response
        return response()->json($data, 200);
    }
    public function sendOtpEmail(Request $request)
    {

        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $email = $request['email'];
        $token = $request['tokentype'];

        $user = DB::select("SELECT * FROM uemka.users WHERE email = '$email'");

        if (!empty($user)) {
            foreach ($user as $u) {
                $notelpon = $u->no_telp;
                $userid = $u->id;
            }

            $Mobile_Phone = $notelpon;
            $tipeotp = $token;
            $createdat = Carbon::now()->format('Y-m-d H:i:s');

            try {

                // Send to database
                $otp = mt_rand(100000, 999999);

                //    send sms gateway
                $curltoken = curl_init();
                $data = array('Username' => env('GM_USER', ''), 'password' => env('GM_PASS', ''));
                curl_setopt($curltoken, CURLOPT_URL, 'https://smsgw.sitama.co.id/api/oauth/token');
                curl_setopt($curltoken, CURLOPT_POST, 1);
                curl_setopt($curltoken, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curltoken, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curltoken, CURLOPT_SSL_VERIFYPEER, false);
                $result = curl_exec($curltoken);
                curl_close($curltoken);

                $token = json_decode($result, true)['access_token'];

                // dd($token);

                // Send Mail
                Mail::send('EmailOtp', ['Nama' => $request['namalengkap'], 'Otp' => $otp], 
                function ($message) use ($request) {
                    $message->from('contact@sitama.co.id', 'GrosirMobil');
                    $message->to($request['email']);
                    $message->subject('Verifikasi OTP UEMKA');
                });
                // End Mail

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://smsgw.sitama.co.id/api/SMS/smssitama",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_POSTFIELDS => "{\n    \"notelp\": \"$Mobile_Phone\",\n    \"textsms\": \"uemka.com%0AYour OTP Code is $otp\",\n    \"desc\": \"Grosir&Mobil\"\n}",
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer " . $token,
                        "Content-Type: application/json"
                    ),
                ));
                // dd($curl);
                $response = curl_exec($curl);
                // dd($response);
                curl_close($curl);
                DB::insert("INSERT INTO `uemka`.`logotp` (`user_id`, `otp`, `tipeotp`, `created_at`) VALUES ('$userid', '$otp', '$tipeotp', '$createdat');");

                $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'OTP success to send'
                ];
            } catch (\Throwable $e) {
                $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'OTP failed to send',
                    'message' => $e(),
                ];

                // return $e;
            }
        } else {
            $data = [
                'ResponseCode' => '99',
                'ResponseDescription' => 'Email not found'
            ];
        }

        // Return response
        return response()->json($data, 200);
    }

    public function changeForgotPassword()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        if ($request) {

            // get request
            $id = $request['userid'];
            $newpassword = $request['newpassword'];
            $confirmpassword = $request['confirmpassword'];


            // Cek LoginID in Database
            $cekLoginID = DB::select("SELECT * FROM uemka.users WHERE id = '$id'");

            // Jika Cek Login tidak kosong
            if (!empty($cekLoginID)) {
                // Jika Password Sama
                if ($newpassword === $confirmpassword) {
                    // Action Hash Password
                    $hashnewpassword = Hash::make($newpassword);
                    try {
                        // Update Password yang terhash
                        DB::update("UPDATE `uemka`.`users` SET `password` = '$hashnewpassword' WHERE (`id` = '$id');");
                        // Return hasil
                        $log = new UsersLog();
                        // $log->log_name = $request->user()->name;
                        $log->log_idlogin = $request['userid'];
                        $log->log_tanggal = Carbon::now()->format('Y-m-d');
                        $log->log_waktu = Carbon::now()->format("H:i:s");
                        $log->log_aktivitas = "Ganti Password";
                        // dd($log);
                        $log->save();

                        return $data = [
                            'ResponseCode' => '00',
                            'ResponseDescription' => 'Berhasil Mengubah Password'
                        ];
                    } catch (\Throwable $th) {
                        // Return Hasil
                        return $th;
                    }
                } else {
                    // Return Hasil
                    $data = [
                        'ResponseCode' => '99',
                        'ResponseDescription' => 'Password Not Same'
                    ];
                }
            }
        } else {
            $data = [
                'ResponseCode' => '99',
                'ResponseDescription' => 'Request Not Found'
            ];
        }

        // return response json
        return response()->json($data, 200);
    }

    public function verificationCode()
    {
        // get content input
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        // get request
        $id = $request['userid'];
        $verificationcode = $request['verificationcode'];

        // $usser = DB::select("SELECT `name` FROM uemka.users WHERE id =",$id);
        $log = new UsersLog();
        // $log->log_name = $usser;
        $log->log_idlogin = $request['userid'];
        $log->log_tanggal = Carbon::now()->format('Y-m-d');
        $log->log_waktu = Carbon::now()->format("H:i:s");
        $log->log_aktivitas = "Verifikasi Kode OTP";
        // dd($log);
        $log->save();
        if ($request) {
            // Cek Login
            $cekLoginEmail = DB::select("SELECT * FROM uemka.logotp WHERE user_id = '$id'");
            if (!empty($cekLoginEmail)) {
                // cek token in database
                $cekVerificationCode = DB::select("SELECT * FROM  uemka.logotp WHERE user_id = '$id' ORDER BY id DESC LIMIT 1");
                if (!empty($cekVerificationCode)) {
                    foreach ($cekVerificationCode as $verif) {
                        // Validasi token
                        if ($verif->otp === $verificationcode) {
                            $update = DB::update("UPDATE `uemka`.`logotp` SET `revoke` = 1 where (`user_id` = '$id' AND `otp` = '$verificationcode' )");
                            if ($update['revoke'] == 1) {
                                return response()->json([
                                    'ResponseCode' => '00',
                                    'ResponseDescrioption' => 'otp sudah expired'
                                ]);
                            }
                            $data = [
                                'ResponseCode' => '00',
                                'ResponseDescription' => 'Verification Success'
                            ];
                        } else {
                            $data = [
                                'ResponseCode' => '99',
                                'ResponseDescription' => 'Verification Failed'
                            ];
                        }
                    }
                } else {
                    $data = [
                        'ResponseCode' => '99',
                        'ResponseDescription' => 'Verification Failed'
                    ];
                }
            } else {
                $data = [
                    'ResponseCode' => '99',
                    'ResponseDescription' => 'Email not Found'
                ];
            }
        }
        // return data
        return response()->json($data, 200);
    }

    public function verifikasipartner(Request $request ,$id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        
        $checkverifikasipartner = Partner::firstWhere('idpartner',$id)->where('verifikasi_partner','=','0')->orderBy('verifikasi_partner', 'DESC');

        if ($checkverifikasipartner) {
            try {
                $partner = Partner::find($id);
                $partner->verifikasi_partner = 1;
                $log = new UsersLog();
                $log->log_name = $request->user()->name;
                $log->log_idlogin = $request->user()->id;
                $log->log_tanggal = Carbon::now()->format('Y-m-d');
                $log->log_waktu = Carbon::now()->format("H:i:s");
                $log->log_aktivitas = "Verifikasi Partner";
                // dd($log);
                $log->save();
                $partner->save();

                return response([
                    'status' => 'OK',
                    'message' => 'Berhasil Verifikasi'
                ],200);
            } catch (\Throwable $th) {
                //throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'Data Tidak Bisa di Verifikasi',
                    'message' => $th->getMessage(),
                ];
            }
        } else {
            return response()->json([
                'status' => 'gagal',
                'message' => 'ID Tidak Di Temukan'
            ],401);
        }
        // try {
        //     $verifikasi = $request['verifikasi_partner'];
        //     $whereveri = $request['idpartner'];
            
        //     DB::update("UPDATE users SET verifikasi_partner = $verifikasi WHERE idpartner = $whereveri");
            
        //     return response()->json([
        //         'message' => 'berhasil di verifikasi'
        //     ],200);
        // } catch (\Throwable $th) {
        //     // throw $th;
        //     return $data = [
        //         'ResponseDescription' => 'Tidak Berhasil Verifikasi',
        //         'message' => $th->getMessage(),
        //     ];
        // }
    }
}

