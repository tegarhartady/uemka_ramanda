<?php

namespace App\Http\Controllers;

use App\JangkaWapin;
use Illuminate\Http\Request;

class JangkaWapinController extends Controller
{
    public function index()
    {
        return response(JangkaWapin::all(),200);
    }

    public function tambahwapin(Request $request)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        // $checkid = JangkaWapin::all()->where('')
        try {
            $jangka = new JangkaWapin;
            $jangka->id_login = $request['id_login'];
            $jangka->id_kalkulator = $request['id_kalkulator'];
            $jangka->id_submit = $request['id_submit'];
            $jangka->tenor = $request['tenor'];
            $jangka->save();
            return response([
                'status' => 'OK',
                'message' => 'Berhasil Tambah Jangka Waktu Pinjaman'
            ],200);
        } catch (\Throwable $th) {
            //throw $th;
            return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Tidak Berhasil Tambah Jangka Waktu Pinjaman',
                'message' => $th->getMessage(),
            ];
        }
    }

    public function edit(Request $request,$id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);

        $checkid = JangkaWapin::firstWhere('id_wapin',$id);
        if ($checkid) {
           try {
            $jangka = JangkaWapin::find($id);
            // $jangka->id_login = $request['id_login'];
            // $jangka->id_kalkulator = $request['id_kalkulator'];
            // $jangka->id_submit = $request['id_submit'];
            $jangka->tenor = $request['tenor'];
            $jangka->save();

            return response([
                'status' => 'OK',
                'message' => 'Berhasil Edit Jangka Waktu Pinjaman',
                'update-data' => $jangka
            ],200);
           } catch (\Throwable $th) {
               //throw $th;
               return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Tidak Edit Tambah Jangka Waktu Pinjaman',
                'message' => $th->getMessage(),
            ];
           }
        }
    }

    public function hapus($id)
    {
        $checkid = JangkaWapin::firstWhere('id_wapin',$id);
        if ($checkid) {
            JangkaWapin::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Wapin Sudah Di Hapus'
            ], 200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Wapin Tidak Ditemukan'
            ], 404);
        }
    }
}
