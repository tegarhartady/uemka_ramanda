<?php

namespace App\Http\Controllers;

use App\Kalkulator;
use App\Submit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KalkulatorController extends Controller
{
    public function index()
    {
        return response()->json(Kalkulator::all(),200);
    }

    public function tambah(Request $request)
    {
        $json = file_get_contents('php://input');

        $request = json_decode($json,true);

        try {
            $kalku = new Kalkulator;
            $kalku->idpartner = $request['idpartner'];
            $kalku->idproduct = $request['idproduct'];
            $kalku->jumlahpinjam = $request['jumlahpinjam'];
            $kalku->tenor = $request['tenor'];
            $kalku->interestrate = $request['interestrate'];
            $kalku->jumlahcicil = $request['jumlahcicil'];
            $kalku->id_login = $request['id_login'];
            $kalku->save();

            return response([
                'status' => 'OK',
                'message' => 'Data Sudah di tambahkan'
            ],200);
        }catch(\Throwable $th)
        {
            return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'Data Tidak Bisa DiTambahkan',
                'message' => $th->getMessage(),
            ];
        }
    }

    public function edit(Request $request, $id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);

        $kalkuid = Kalkulator::firstWhere('idkalkulator', $id);
        if($kalkuid){
            try{
            $kalku = Kalkulator::find($id);
            $kalku->idpartner = $request['idpartner'];
            $kalku->idproduct = $request['idproduct'];
            $kalku->jumlahpinjam = $request['jumlahpinjam'];
            $kalku->tenor = $request['tenor'];
            $kalku->interestrate = $request['interestrate'];
            $kalku->jumlahcicil = $request['jumlahcicil'];
            $kalku->id_login = $request['id_login'];
            $kalku->save();

            return response([
                'status' => 'OK',
                'message' => 'Data Sudah di Update'
            ],200);
            }catch(\Throwable $th)
            {
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'Data Tidak Bisa Diupdate',
                    'message' => $th->getMessage(),
                ];
            }
        } else {
            return response()->json([
                'status' => 'gagal',
                'message' => 'ID Tidak Di Temukan'
            ],401);
        }
    }

    public function hapus($id)
    {
        $kalkuid = Kalkulator::firstWhere('idkalkulator',$id);

        if ($kalkuid) {
            Kalkulator::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Tdak Berasil Di Temukan'
            ],401);
        }
    }

    public function menghitung()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        try {
        $interestrate = $request['interestrate'];
        $tenor = $request['tenor'];
        $jumlahpinjam = $request['jumlahpinjam'];
        $kal = DB::SELECT("call uemka.spPAYMENT('$interestrate','$tenor','$jumlahpinjam',0,0)");
        dd($kal);

        return response([
            'message' => 'Berhasil menghitung',
            'kalkulasi' => $kal
        ],200);
        }catch(\Throwable $th)
        {
        return $data = [
            'ResponseCode' => '00',
            'ResponseDescription' => 'Data Tidak Bisa DiTambahkan',
            'message' => $th->getMessage(),
        ];
        }
    }
}
