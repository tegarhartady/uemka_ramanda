<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return response()->json(Product::all(),200);
    }

    public function tambah_product(Request $request)
    {
        $json = file_get_contents('php://input');

        $request = json_decode($json,true);

        $tambah = Product::all()->where('kode_product','=',$request['kode_product']);

        foreach($tambah as $a)
        {
            $kode = $a->kode_product;
        }

        if (!empty($kode)) {
            return $data = [
                'ResponseCode' => '99',
                'ResponseDirection' => 'Kode Produk Sudah Terdaftar'
            ];
        } else {
            try {
                $date = date("Y-M-d H:i:s");

                $product = new Product;
                $product->namaperusahaan = $request['namaperusahaan'];
                $product->kode_product = $request['kode_product'];
                $product->nama_product = $request['nama_product'];
                $product->desc_product = $request['desc_product'];
                $product->persyaratan_product = $request['persyaratan_product'];
                $product->pic_email = $request['pic_email'];
                $product->nohp_pic = $request['nohp_pic'];
                $product->jenis_product = $request['jenis_product'];
                $product->created_at = $date; 
                $product->idpartner = $request['idpartner'];
                $product->save();

                return response([
                    'status' => 'OK',
                    'message' => 'Data Product Berhasil Terdaftar'
                ]);
            } catch (\Throwable $th) {
                //throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescreption' => 'Produk Gagal Terdaftar',
                    'Message' => $th->getMessage()
                ];
            }
        }
    }

    public function update_product(Request $request, $id)
    {
        $json = file_get_contents('php://input');
        // merubah json ke string
        $request = json_decode($json, true);

        $produkid = Product::firstWhere('idproduct', $id);

        if ($produkid) {
            $product = Product::find($id);
            $product->nama_perusaahaan = $request['namaperusahaan'];
            $product->kode_product = $request['kode'];
            $product->nama_product = $request['nama_product'];
            $product->desc_product = $request['desc_product'];
            $product->persyaratan_product = $request['persyaratan_product'];
            $product->pic_email = $request['pic_email'];
            $product->nohp_pic = $request['nohp_pic'];
            $product->jenis_product = $request['jenis_product'];
            $product->save();

            return response([
                'status' => 'OK',
                'message' => 'Data Product Berhasil di Update',
                'update-data' => $product
            ],200);
        } else {
            return response()->json([
                'Status' => 'Gagal',
                'Message' => 'Data Produk Tidak Berhasil Di Update',
            ], 404);
        }
    }
    public function destroy($id)
    {
        $check_id = Product::firstWhere('idproduct',$id);
        if ($check_id) {
            Product::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Produk Sudah Di Hapus'
            ], 200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Produk Tidak Ditemukan'
            ], 404);
        }
    }

    public function productpartner($id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);
        return response()->json(Product::all()->where('idpartner','=',$id),200);
    }
    public function rate($id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);
        return response()->json(Product::all()->where('idproduct','=',$id),200);
    }

    public function productpartnerrr($id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);
        return response()->json(Product::all()->where('idpartner','=',$id)->limit(3),200);
    }
}
