<?php

namespace App\Http\Controllers;

use App\Promosi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Laravel\Ui\Presets\React;

class PromosiController extends Controller
{
    function slugify($text)
    {
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    // trim
    $text = trim($text, '-');

    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);

    // lowercase
    $text = strtolower($text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
    }

    public function index()
    {
        return response()->json(Promosi::all(),200);
        // return $this->slugify("Promo Semangat Akhir Tahun Dana Cair Express dengan BPKB Mobil");
        
    }
    public function whereslug()
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);
        return response()->json(Promosi::all()->where('slug','=',$request['slug'])->first(),200);
        // return $this->slugify("Promo Semangat Akhir Tahun Dana Cair Express dengan BPKB Mobil");
        
    }

    public function addpromosi(Request $request)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        $judul = Promosi::all()->where('judul_promosi','=',$request['judul_promosi']);

        foreach ($judul as $a) {
            $juduls = $a->judul_promosi;
        }

        if (!empty($juduls)) {
            return $data = [
                'ResponseCode' => '99',
                'ResponseDirection' => 'judul Promosi Sudah Terdaftar'
            ];
        } else {
            try {
                $promosi = new Promosi;
                $promosi->judul_promosi = $request['judul_promosi'];
                $promosi->slug = $this->slugify($request['judul_promosi']);
                // $promosi->image_promosi = $request['image_promosi'];
                $photo    = str_replace('data:image/png;base64,', '', $request['image_promosi']);
                $photoktp = time() . '_' . uniqid() . '.' . 'png';
                $defaultfile = '/' . env('INPUT_PHOTO', '') . '/' . 'Promosi';
                Storage::disk('sftp')->put($defaultfile . $photoktp, base64_decode($photo));
                $promosi->image_promosi = $photoktp;
                $promosi->hyperlink_promosi = $request['hyperlink_promosi'];
                $promosi->by_promosi = $request['by_promosi'];
                $promosi->isi_promosi = $request['isi_promosi'];
                // $this->slugify("Promo Semangat Akhir Tahun Dana Cair Express dengan BPKB Mobil");
                // $promosi->tanggal_promosi = $request['tanggal_promosi'];
                $promosi->save();

                return response()->json([
                    'Message' => 'Promosi Berhasil tersimpan',
                    'data' => $promosi
                ],200);
            } catch (\Throwable $th) {
                //throw $th;

                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDirection' => 'Promosi Gagal Disimpan',
                    'Message' => $th->getMessage(),
                ];
            }
        }
    }

    public function update_promosi(Request $request, $id)
    {
        $json = file_get_contents('php://input');
        $request  = json_decode($json, true);

        $promosiid = Promosi::firstWhere('idpromosi', $id);

        if ($promosiid) {
            $promosi = Promosi::find($id);
            $promosi->judul_promosi = $request['judul_promosi'];
            $promosi->image_promosi = $request['image_promosi'];
            $promosi->hyperlink_promosi = $request['hyperlink_promosi'];
            $promosi->by_promosi = $request['by_promosi'];
            // $promosi->tanggal_promosi = $request['tanggal_promosi'];
            $promosi->save();

            return response([
                'status' => 'OK',
                'message' => 'Data Promosi Berhasil Di Update',
                'update-data' => $promosi
            ],200);
        } else {
            return response()->json([
                'Status' => 'Gagal',
                'Message' => 'Data Produk Tidak Berhasil Di Update',
            ], 404);
        }
    }

    public function destroy($id)
    {
        $check_id = Promosi::firstWhere('idpromosi',$id);
        if ($check_id) {
            Promosi::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Promosi Sudah Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'Message' => 'Data Promosi Tidak Ditemukan'
            ],401);
        }
    }
    public function perlimit()
    {
        return response()->json(Promosi::limit(3)->orderBy('created_at','desc')->get(),200);
    }
}
