<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListofController extends Controller
{
    public function listpartner()
    {
        $listpart = DB::table('profile_lembaga')
                    ->select('nama_perusahaan','alamat_perusahaan','website_perusahaan','nama_lengkap_pic','alamat_pic','created_at')
                    ->get();
        return response()->json($listpart);
    }
}
