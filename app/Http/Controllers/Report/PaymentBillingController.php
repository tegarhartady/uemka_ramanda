<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentBillingController extends Controller
{
    public function getall()
    {
        $payment = DB::table('partner')
                    ->join('users','users.id','=','partner.id_login')
                    ->join('kalkulator','kalkulator.id_login','=','partner.id_login')
                    ->select('partner.nama_partner','users.name','kalkulator.jumlahpinjam','kalkulator.tenor','kalkulator.jumlahcicil')
                    ->get();
        return response()->json($payment);
    }
}
