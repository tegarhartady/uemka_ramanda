<?php

namespace App\Http\Controllers;

use App\Testi;
use Illuminate\Http\Request;

class TestiController extends Controller
{
    public function index()
    {
        return response()->json(Testi::all(),200);
    }
    public function perlimit()
    {
        return response()->json(Testi::limit(3)->orderBy('created_at','desc')->get(),200);
    }

    public function add_testi(Request $request)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json, true);

        try {
            $testi = new Testi;
            $testi->nama_testi = $request['nama_testi'];
            $testi->email_testi = $request['email_testi'];
            $testi->judul_testi = $request['judul_testi'];
            $testi->isi_testi = $request['isi_testi'];
            $testi->usaha_testi = $request['usaha_testi'];
            // dd($testi);
            $testi->save();

            return response([
                'status' => 'OK',
                'message' => 'Terimakasih Sudah Memberikan Testimoni'
            ],200);
        } catch (\Throwable $th) {
            //throw $th;
            return $data = [
                'ResponseCode' => '00',
                'ResponseDescription' => 'User Gagal Terdaftar',
                'message' => $th->getMessage(),
            ];
        }
    }

    public function edit_testi(Request $request,$id)
    {
        $json = file_get_contents('php://input');
        $request = json_decode($json,true);

        $testiid = Testi::firstWhere('idtesti',$id);

        if ($testiid) {
            try {
            $testimoni = Testi::find($id);
            $testimoni->nama_testi = $request['nama_testi'];
            $testimoni->email_testi = $request['email_testi'];
            $testimoni->judul_testi = $request['judul_testi'];
            $testimoni->isi_testi = $request['isi_testi'];
            $testimoni->usaha_testi = $request['usaha_testi'];
            $testimoni->save();

            return response([
                'status' => 'OK',
                'message' => 'Data Testi Berhasil Di Ubah',
                'update-data' => $testimoni
            ],200);
            } catch (\Throwable $th) {
                // throw $th;
                return $data = [
                    'ResponseCode' => '00',
                    'ResponseDescription' => 'testi Gagal Di tambahkan',
                    'message' => $th->getMessage(),
                ];
            }
        } else {
            return response()->json([
                'status' => 'Gagal',
                'message' => 'id Testi Tidak di temukan'
            ],401);
        }
    }

    public function hapus($id)
    {
        $checkid = Testi::firstWhere('idtesti',$id);

        if ($checkid) {
            Testi::destroy($id);
            return response([
                'status' => 'OK',
                'message' => 'Data Testi Berhasil Di Hapus'
            ],200);
        } else {
            return response([
                'status' => 'Gagal',
                'message' => 'Data Testi Tidak Di Temukan'
            ],401);
        }
    }
}
