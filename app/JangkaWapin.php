<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JangkaWapin extends Model
{
    protected $table = 'uemka.jangka_wapin';
    protected $primaryKey = 'id_wapin';
    protected $fillable = [
        'id_wapin','id_login','id_kalkulator','id_submit','tenor','created_at','updated_at'
    ];
}