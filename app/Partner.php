<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $table = "uemka.partner";

    protected $fillable = [
        'idpartner','nama_partner','email_partner','alamat_partner','nohp_partner','website_partner','jenis_partner','desc_partner','visimisi_partner','id_login','updated_at','verifikasi_partner'
    ];  

    protected $primaryKey = 'idpartner';
}
