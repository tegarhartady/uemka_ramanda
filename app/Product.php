<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "uemka.product";

    protected $fillable = [
        'idproduct','idpartner','namaperusahaan','kode_product','nama_product','desc_product','persyaratan_product','pic_email','nohp_pic','jenis_product','updated_at','created_at','interestrate'
    ];

    protected $primaryKey = 'idproduct';
}
