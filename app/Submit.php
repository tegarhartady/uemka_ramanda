<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submit extends Model
{
    protected $table = 'uemka.submit_application';
    protected $fillable = [
        'submit_id','kalkulator_id','nama_submit','email_submit','tenor_submit','updated_at','created_at','approve'
    ];
    protected $primaryKey = 'submit_id';
}
