<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testi extends Model
{
    protected $table = 'uemka.testimoni';
    protected $fillable = [
        'idtesti','nama_testi','email_testi','judul_testi','isi_testi','usaha_testi','updated_at','created_at'
    ];
    protected $primaryKey = 'idtesti';
}
