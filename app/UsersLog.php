<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersLog extends Model
{
    protected $table = "uemka.users_log";
    protected $fillable = [
        'logid','log_idlogin','log_name','log_tanggal','log_waktu'
    ]; 

    protected $primaryKey = 'logid';
}
