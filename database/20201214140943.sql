/*
MySQL Backup
Database: apiuemka
Backup Time: 2020-12-14 14:09:44
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `apiuemka`.`article`;
DROP TABLE IF EXISTS `apiuemka`.`artikels`;
DROP TABLE IF EXISTS `apiuemka`.`failed_jobs`;
DROP TABLE IF EXISTS `apiuemka`.`logotp`;
DROP TABLE IF EXISTS `apiuemka`.`migrations`;
DROP TABLE IF EXISTS `apiuemka`.`oauth_access_tokens`;
DROP TABLE IF EXISTS `apiuemka`.`oauth_auth_codes`;
DROP TABLE IF EXISTS `apiuemka`.`oauth_clients`;
DROP TABLE IF EXISTS `apiuemka`.`oauth_personal_access_clients`;
DROP TABLE IF EXISTS `apiuemka`.`oauth_refresh_tokens`;
DROP TABLE IF EXISTS `apiuemka`.`partner`;
DROP TABLE IF EXISTS `apiuemka`.`password_resets`;
DROP TABLE IF EXISTS `apiuemka`.`product`;
DROP TABLE IF EXISTS `apiuemka`.`profile_lembaga`;
DROP TABLE IF EXISTS `apiuemka`.`promosi`;
DROP TABLE IF EXISTS `apiuemka`.`users`;
CREATE TABLE `article` (
  `idarticle` int(11) NOT NULL AUTO_INCREMENT,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `judul_article` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `foto_article` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `by_upload` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `isi_article` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idarticle`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
CREATE TABLE `artikels` (
  `idartikel` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idartikel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `logotp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `otp` varchar(255) NOT NULL,
  `tipeotp` varchar(255) NOT NULL,
  `revoke` varchar(255) DEFAULT '0',
  `created_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `partner` (
  `idpartner` int(11) NOT NULL AUTO_INCREMENT,
  `nama_partner` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `alamat_partner` longtext COLLATE latin1_bin,
  `nohp_partner` varchar(18) COLLATE latin1_bin DEFAULT NULL,
  `website_partner` varchar(105) COLLATE latin1_bin DEFAULT NULL,
  `jenis_partner` varchar(45) COLLATE latin1_bin DEFAULT NULL,
  `desc_partner` longtext COLLATE latin1_bin,
  `visimisi_partner` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idpartner`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `idpartner` int(11) DEFAULT NULL,
  `namaperusahaan` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `kode_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `nama_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `desc_product` longtext COLLATE latin1_bin,
  `persyaratan_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `pic_email` varchar(155) COLLATE latin1_bin DEFAULT NULL,
  `nohp_pic` varchar(18) COLLATE latin1_bin DEFAULT NULL,
  `jenis_product` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idproduct`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
CREATE TABLE `profile_lembaga` (
  `id_perusahaan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(45) NOT NULL,
  `email_perusahaan` varchar(255) NOT NULL,
  `password_perusahaan` varchar(255) NOT NULL,
  `alamat_perusahaan` varchar(255) DEFAULT NULL,
  `website_perusahaan` varchar(255) DEFAULT NULL,
  `nama_lengkap_pic` varchar(255) DEFAULT NULL,
  `nomor_handphone_pic` varchar(20) DEFAULT NULL,
  `alamat_pic` varchar(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_perusahaan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
CREATE TABLE `promosi` (
  `idpromosi` int(11) NOT NULL,
  `id_login` varchar(150) COLLATE latin1_bin DEFAULT NULL,
  `judul_promosi` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  `by_promosi` varchar(155) COLLATE latin1_bin DEFAULT NULL,
  `tanggal_promosi` datetime DEFAULT NULL,
  `hyperlink_promosi` varchar(255) COLLATE latin1_bin DEFAULT NULL,
  PRIMARY KEY (`idpromosi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_bin;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sosmed` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
BEGIN;
LOCK TABLES `apiuemka`.`article` WRITE;
DELETE FROM `apiuemka`.`article`;
INSERT INTO `apiuemka`.`article` (`idarticle`,`id_login`,`judul_article`,`foto_article`,`updated_at`,`by_upload`,`isi_article`,`created_at`) VALUES (1, NULL, 'Test Lagi', 'test.jpg', '2020-12-08 10:11:06', 'Anonymous', 'Ini adalah testing', '2020-12-08 10:03:52'),(2, NULL, 'Testing', 'test.jpg', '2020-12-08 10:09:48', 'Anonymous', 'Ini adalah testing', '2020-12-08 10:09:48'),(3, NULL, 'Testing', 'test.jpg', '2020-12-08 10:11:20', 'Anonymous', 'Ini adalah testing', '2020-12-08 10:11:20');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`artikels` WRITE;
DELETE FROM `apiuemka`.`artikels`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`failed_jobs` WRITE;
DELETE FROM `apiuemka`.`failed_jobs`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`logotp` WRITE;
DELETE FROM `apiuemka`.`logotp`;
INSERT INTO `apiuemka`.`logotp` (`id`,`user_id`,`otp`,`tipeotp`,`revoke`,`created_at`,`updated_at`) VALUES (1, 10, '306590', 'Forgot Password', '1', '2020-12-14 10:52:27', '2020-12-14 10:52:27'),(2, 10, '721878', 'Forgot Password', '1', '2020-12-14 10:52:27', '2020-12-14 10:52:27'),(3, 10, '903776', 'Forgot Password', '1', '2020-12-14 10:52:27', '2020-12-14 10:52:27'),(4, 10, '680692', 'Forgot Password', '1', '2020-12-14 10:52:27', '2020-12-14 10:52:27'),(5, 10, '263606', 'Forgot Password', '1', '2020-12-14 10:52:27', '2020-12-14 10:52:27'),(6, 3, '682927', 'Forgot Password', '0', '2020-12-14 03:41:53', NULL),(7, 10, '215624', 'Forgot Password', '1', '2020-12-14 13:17:55', '2020-12-14 13:17:55'),(8, 10, '328807', 'Forgot Password', '1', '2020-12-14 13:36:33', '2020-12-14 13:36:33');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`migrations` WRITE;
DELETE FROM `apiuemka`.`migrations`;
INSERT INTO `apiuemka`.`migrations` (`id`,`migration`,`batch`) VALUES (17, '2014_10_12_000000_create_users_table', 1),(18, '2014_10_12_100000_create_password_resets_table', 1),(19, '2016_06_01_000001_create_oauth_auth_codes_table', 1),(20, '2016_06_01_000002_create_oauth_access_tokens_table', 1),(21, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),(22, '2016_06_01_000004_create_oauth_clients_table', 1),(23, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),(24, '2019_08_19_000000_create_failed_jobs_table', 1),(25, '2020_12_08_081334_create_artikells_table', 2);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`oauth_access_tokens` WRITE;
DELETE FROM `apiuemka`.`oauth_access_tokens`;
INSERT INTO `apiuemka`.`oauth_access_tokens` (`id`,`user_id`,`client_id`,`name`,`scopes`,`revoked`,`created_at`,`updated_at`,`expires_at`) VALUES ('058e728dcf45fb3578d547df51e2b4c8137cea291086b3681d80fcc1b5cf718bb52309f2e56a0173', 7, 1, 'Personal Access Token', '[]', 0, '2020-12-04 09:34:07', '2020-12-04 09:34:07', '2021-12-04 09:34:07'),('1042fca9aaa3764474f0981496b41daebd241dc2b0ffdf7d95631b5a38d728dffd6966317822e3fa', 9, 3, 'Personal Access Token', '[]', 0, '2020-12-09 15:13:13', '2020-12-09 15:13:13', '2021-12-09 15:13:13'),('1613e36a01f39a492492201a49ffffe89a9ece8c76a075d617a36ceb3c535e7ec1208259ed9c2db2', 10, 3, 'Personal Access Token', '[]', 0, '2020-12-12 15:26:24', '2020-12-12 15:26:24', '2021-12-12 15:26:24'),('175c558cbf78cd3913ce2575f9af7084f384246e9d2a8c5f2a43a4b1e9a07dd872313a46b14e7662', 9, 3, 'Personal Access Token', '[]', 0, '2020-12-12 16:31:17', '2020-12-12 16:31:17', '2021-12-12 16:31:17'),('17827437b65663d7428b2cefccd228d5496697e64aad505f8268d7608b054a58c453cfcc270ac6ba', 7, 1, 'Personal Access Token', '[]', 1, '2020-12-08 02:30:55', '2020-12-08 02:30:55', '2021-12-08 02:30:55'),('1b41b747b33575cc501c219bca06b623eba7cbd6f1691affc00862d80a15b7d4e095c79d4d4861d0', 1, 1, 'Personal Access Token', '[]', 0, '2020-12-04 06:33:23', '2020-12-04 06:33:23', '2021-12-04 06:33:23'),('2d0ef3754a5572489c488838d1ec4c1028c10cf35bc01dab35906257e5ccba36329112f40e7b4af9', 2, 1, 'Personal Access Token', '[]', 1, '2020-12-04 07:20:33', '2020-12-04 07:20:33', '2021-12-04 07:20:33'),('305f5ebbb28ce246fc4a3053bc533951592b31ebcec4ed50c8aaab7fe5ac0d2a1a70ef4c54de4e16', 7, 1, 'Personal Access Token', '[]', 1, '2020-12-07 08:34:26', '2020-12-07 08:34:26', '2021-12-07 08:34:26'),('4c2152b0307fb798b94531b15dd114e0c69c7425baffa25dc1663195caee936184e82ae801382e79', 6, 1, 'Personal Access Token', '[]', 0, '2020-12-04 09:31:22', '2020-12-04 09:31:22', '2021-12-04 09:31:22'),('6286d1037ba4ed970263542b268bc40f650294e2e23c9f825a732b17366ed4808f760e0f810e91cd', 10, 3, 'Personal Access Token', '[]', 1, '2020-12-14 06:40:43', '2020-12-14 06:40:43', '2021-12-14 06:40:43'),('7b2accd0a0d17a13af6e53b3f18378aed7a15bc23e5fa1cf386bf1d6a49e2def7bcbf6d8114c44d7', 9, 3, 'Personal Access Token', '[]', 0, '2020-12-10 09:55:18', '2020-12-10 09:55:18', '2021-12-10 09:55:18'),('83e78b17aa676e202d5cd4d2b757272b404c06f579ff9dc20d0b0d4f33f85531ce3b3b4c71f17dfc', 9, 1, 'Personal Access Token', '[]', 0, '2020-12-08 07:43:15', '2020-12-08 07:43:15', '2021-12-08 07:43:15'),('86f0514a5faf0d0bda71efab2724d11fa700ac7479e68ab231674ea65d27ad1af43f6b1a0d4d402a', 10, 3, 'Personal Access Token', '[]', 0, '2020-12-14 06:34:45', '2020-12-14 06:34:45', '2021-12-14 06:34:45'),('ade73ba01b8275c1c4ba3726b40a0005e7528781e1bc96d8986d1e15d7e1f7dcca99b18a074497ce', 10, 3, 'Personal Access Token', '[]', 1, '2020-12-14 06:54:12', '2020-12-14 06:54:12', '2021-12-14 06:54:12'),('c11b220d46ed5d6abd2210ea9fe843661fef4f3cc8e86b2a8993b7ff296945727d25e480f8313a79', 9, 3, 'Personal Access Token', '[]', 0, '2020-12-10 09:02:02', '2020-12-10 09:02:02', '2021-12-10 09:02:02'),('c59964f4d81eadbdbadbbd2330c5d85e784780885ef55d7c854efe031528af1cbac377f618103133', 7, 1, 'Personal Access Token', '[]', 1, '2020-12-07 03:01:43', '2020-12-07 03:01:43', '2021-12-07 03:01:43'),('e2f35ece423f222750e38a0329aed8f64b672e2a6020f00231b5de5ee6a916de240c3e9612e90c1e', 7, 1, 'Personal Access Token', '[]', 1, '2020-12-07 08:18:41', '2020-12-07 08:18:41', '2021-12-07 08:18:41');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`oauth_auth_codes` WRITE;
DELETE FROM `apiuemka`.`oauth_auth_codes`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`oauth_clients` WRITE;
DELETE FROM `apiuemka`.`oauth_clients`;
INSERT INTO `apiuemka`.`oauth_clients` (`id`,`user_id`,`name`,`secret`,`provider`,`redirect`,`personal_access_client`,`password_client`,`revoked`,`created_at`,`updated_at`) VALUES (1, NULL, 'Laravel Personal Access Client', 'qJq3WlhYtHKgZuW1SgldEwWguoEUdsFTntHPxwCo', NULL, 'http://localhost', 1, 0, 0, '2020-12-04 06:23:34', '2020-12-04 06:23:34'),(2, NULL, 'Laravel Password Grant Client', 'yxAdeXI1S1D0dwinC6Ylj6YhacvI5VCGJ0oxEhhM', 'users', 'http://localhost', 0, 1, 0, '2020-12-04 06:23:34', '2020-12-04 06:23:34'),(3, NULL, 'Laravel Personal Access Client', 'RMDwioEQLoQkyLWKhLVzdlHnM6VnaYOXmmBP71kZ', NULL, 'http://localhost', 1, 0, 0, '2020-12-09 11:52:44', '2020-12-09 11:52:44'),(4, NULL, 'Laravel Password Grant Client', 'P8lFxUJfbwYpLfPrWkj8L3ZbPdNmeoGDuvNFW0e6', 'users', 'http://localhost', 0, 1, 0, '2020-12-09 11:52:44', '2020-12-09 11:52:44');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`oauth_personal_access_clients` WRITE;
DELETE FROM `apiuemka`.`oauth_personal_access_clients`;
INSERT INTO `apiuemka`.`oauth_personal_access_clients` (`id`,`client_id`,`created_at`,`updated_at`) VALUES (1, 1, '2020-12-04 06:23:34', '2020-12-04 06:23:34'),(2, 3, '2020-12-09 11:52:44', '2020-12-09 11:52:44');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`oauth_refresh_tokens` WRITE;
DELETE FROM `apiuemka`.`oauth_refresh_tokens`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`partner` WRITE;
DELETE FROM `apiuemka`.`partner`;
INSERT INTO `apiuemka`.`partner` (`idpartner`,`nama_partner`,`alamat_partner`,`nohp_partner`,`website_partner`,`jenis_partner`,`desc_partner`,`visimisi_partner`,`id_login`,`created_at`,`updated_at`) VALUES (1, 'Tegar', 'parungpanjang', '012382341', 'website_partner', 'Koperasi', 'Sebagau Test', 'Menjadikan koperasi yang unggul', NULL, '2020-12-07 08:26:42', '2020-12-08 06:58:04'),(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-07 09:53:08', '2020-12-08 02:29:27'),(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-08 02:07:51', '2020-12-08 02:29:19'),(5, 'Masaki suda', 'Jakartasss', '123123123123', 'www.asdf.com', 'Koperasi', 'parungpanjang', 'mengkedepankan gaya hidup dengan IT', NULL, '2020-12-08 02:29:46', '2020-12-08 02:29:46'),(6, 'Tegar Hartadi', 'parungpanjang', '012382341', 'website_partner', 'Koperasi', 'Sebagau Test', 'Menjadikan koperasi yang unggul', NULL, '2020-12-08 06:55:07', '2020-12-08 06:55:07'),(7, 'Tegar Hartadi', 'parungpanjang', '987654321', 'website_partner', 'Koperasi', 'Sebagau Test', 'Menjadikan koperasi yang unggul', NULL, '2020-12-10 09:39:21', '2020-12-10 09:39:21'),(8, 'Tegar Hartadi', 'parungpanjang', '213213', 'website_partner', 'Koperasi', 'Sebagau Test', 'Menjadikan koperasi yang unggul', '9', '2020-12-10 09:55:37', '2020-12-10 09:55:37');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`password_resets` WRITE;
DELETE FROM `apiuemka`.`password_resets`;
INSERT INTO `apiuemka`.`password_resets` (`email`,`token`,`created_at`) VALUES ('t.hartady21@gmail.com', 'YzDEevicBBlNqAKebnMFMeZGjX0UW0yCtP32VaLCqsz78HkmdRwvdKdDWwFW', '2020-12-14 02:29:06'),('t.hartady21@gmail.com', 'F4XE1Q5knjY9nsLCezP3924idQTF6R48iZBguYVzV1KLBMXqJmXAsIhWi06l', '2020-12-14 02:31:48'),('t.hartady21@gmail.com', 'zxKNoQyK0KwpXsliHCCnqSzSXgQYELj9rHcVOWNHiZrnNjlkS1js5xwBA7Fq', '2020-12-14 02:33:40');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`product` WRITE;
DELETE FROM `apiuemka`.`product`;
INSERT INTO `apiuemka`.`product` (`idproduct`,`idpartner`,`namaperusahaan`,`kode_product`,`nama_product`,`desc_product`,`persyaratan_product`,`pic_email`,`nohp_pic`,`jenis_product`,`updated_at`,`created_at`) VALUES (1, NULL, 'Rozortech', '123123', 'Yang Terkuat', 'Bakal Kuat Semalaman', 'Jangan di gunakan di siang hari', 'baihakitanjung@gmail.com', '91231233412', 'BPR', '2020-12-10 10:41:45', '2020-12-10 10:41:45'),(2, NULL, 'Rozortech', '123122', 'Yang Terkuat', 'Bakal Kuat Semalaman', 'Jangan di gunakan di siang hari', 'baihakitanjung@gmail.com', '91231233412', 'BPR', '2020-12-10 11:01:05', '2020-12-10 11:01:05');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`profile_lembaga` WRITE;
DELETE FROM `apiuemka`.`profile_lembaga`;
INSERT INTO `apiuemka`.`profile_lembaga` (`id_perusahaan`,`nama_perusahaan`,`email_perusahaan`,`password_perusahaan`,`alamat_perusahaan`,`website_perusahaan`,`nama_lengkap_pic`,`nomor_handphone_pic`,`alamat_pic`,`updated_at`,`created_at`) VALUES (1, 'Instagram Univercity', 'lili@fb.com', '$2y$10$11zwodVl4YHmr8BkXLp6h.NpeXEAO5Q6Tk1FxmpFS3AbIIarZxnXe', NULL, 'Instagram.com', 'Tegar Hartady', NULL, 'parungpanjang', '2020-12-04 07:14:10', '2020-12-04 07:14:09'),(2, 'Instagram Univercity', 'garr@fb.com', '$2y$10$l50Fvmxzdl/ksB.LKmn58u3mTcpADHB438yFf3TjucWhw8I/ghwkC', NULL, 'Instagram.com', 'Tegar Hartady', NULL, 'parungpanjang', '2020-12-04 07:22:30', '2020-12-04 07:22:30'),(3, 'Instagram Univercity', 'pair@mail.com', '$2y$10$POHVdk01WTFPTCpF1KOf9eliWBY5si/qR7OYAENtuxyqqwZ.FaYp.', NULL, 'Instagram.com', 'Tegar Hartady', NULL, 'parungpanjang', '2020-12-04 07:56:56', '2020-12-04 07:56:56'),(4, 'Instagram Univercity', 'andi@mail.com', '$2y$10$FYrT4glhTr9iYO7tjHj5LOwsFH2Rj.4e8ddF/h55voDKLonZKELpm', NULL, 'Instagram.com', 'Tegar Hartady', NULL, 'parungpanjang', '2020-12-04 09:26:48', '2020-12-04 09:26:48'),(5, 'Instagram Univercity', 'hartady@mail.com', '$2y$10$1mLpQdjYccpMdDCaEd2U4OTTnshutJ6MSXAwkJVW74LDeHCTkdad2', NULL, 'Instagram.com', 'Tegar Hartady', NULL, 'parungpanjang', '2020-12-04 09:33:40', '2020-12-04 09:33:40');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`promosi` WRITE;
DELETE FROM `apiuemka`.`promosi`;
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `apiuemka`.`users` WRITE;
DELETE FROM `apiuemka`.`users`;
INSERT INTO `apiuemka`.`users` (`id`,`name`,`email`,`email_verified_at`,`password`,`no_telp`,`sosmed`,`level`,`remember_token`,`created_at`,`updated_at`,`pekerjaan`,`access_token`) VALUES (1, 'Instagram Univercity', 'lili@fb.com', NULL, '$2y$10$Ef.Zveq1ptTGcbFs46lKveJNVFNWzITX/2sAL3WsK3wp/Rg68QAIC', '085156257614', 'Instagram.com', 3, NULL, '2020-12-04 07:14:10', '2020-12-04 07:14:10', NULL, NULL),(2, 'takikun1', 'sisi@mail.com', NULL, '$2y$10$wB1ZI/WZ/uFdTIUmcZ0H3ONmfOcZStuHOd/5Go/XteZpguqT1/hWa', '085156257614', 'Tegar Hartady', 2, NULL, '2020-12-04 07:20:18', '2020-12-04 07:20:18', NULL, NULL),(3, 'Instagram Univercity', 'garr@fb.com', NULL, '$2y$10$r9sTo2U1kR1iWViXvWeMCe0T5DFEHqHO5TWgAu6CPekexHgvfXWuq', '085156257614', 'Instagram.com', 3, NULL, '2020-12-04 07:22:30', '2020-12-04 07:22:30', NULL, NULL),(4, 'kuku', 'kuku@mail.com', NULL, '$2y$10$nRuawg4sD4M9ea3YRIPeA.OY99a5.QggH1fWSsXK6dn9y2Ym5r8lS', '085156257614', 'Tegar Hartady', 2, NULL, '2020-12-04 07:55:12', '2020-12-04 07:55:12', NULL, NULL),(5, 'Instagram Univercity', 'pair@mail.com', NULL, '$2y$10$92Rff.qwKym6u3Waij3NxuXMzXl9rY0ThKh9o.R3lh6jg.IW9/bIm', '085156257614', 'Instagram.com', 3, NULL, '2020-12-04 07:56:56', '2020-12-04 07:56:56', NULL, NULL),(6, 'Instagram Univercity', 'andi@mail.com', NULL, '$2y$10$FYrT4glhTr9iYO7tjHj5LOwsFH2Rj.4e8ddF/h55voDKLonZKELpm', '085156257614', 'Instagram.com', 2, NULL, '2020-12-04 09:26:48', '2020-12-04 09:26:48', NULL, NULL),(7, 'Instagram Univercity', 'hartady@mail.com', NULL, '$2y$10$1mLpQdjYccpMdDCaEd2U4OTTnshutJ6MSXAwkJVW74LDeHCTkdad2', '085156257614', 'Instagram.com', 3, NULL, '2020-12-04 09:33:40', '2020-12-04 09:33:40', NULL, NULL),(8, 'kuku', 'baihaki@mail.com', NULL, '$2y$10$0ptlSXaEI/mabrbo5XJU5OAUHMYju1dvIh59rIvIv67W2SGr3Ik0G', '085156257614', 'Tegar Hartady', 2, NULL, '2020-12-07 08:37:30', '2020-12-07 08:37:30', NULL, NULL),(9, 'Kenshin', 'kenshin@mail.com', NULL, '$2y$10$s6DFQjtXFtWtNwsb6TBZtuFON7pEJGflZfajJRLDtvNRrQn40K9J6', '085156257614', 'battosai', 2, NULL, '2020-12-08 07:10:24', '2020-12-08 07:10:24', NULL, NULL),(10, 'garrr', 't.hartady21@gmail.com', NULL, '$2y$10$T/KNpkQu4Bs3JB.wgluRJOLZWGpJMqlqebcUQMk4P6y5Sn1jnn74e', '+6285156257614', 'battosai', 2, NULL, '2020-12-09 15:14:16', '2020-12-09 15:14:16', NULL, NULL);
UNLOCK TABLES;
COMMIT;
