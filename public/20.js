(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[20],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/cms/profile.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/cms/profile.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _layouts_template_Sidebar_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../layouts/template/Sidebar.vue */ "./resources/js/layouts/template/Sidebar.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    Sidebar: _layouts_template_Sidebar_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "wrapper" } }, [
      _c(
        "ul",
        {
          staticClass: "navbar-nav bg-dark sidebar sidebar-dark accordion",
          attrs: { id: "accordionSidebar" }
        },
        [
          _c("br"),
          _vm._v(" "),
          _c(
            "a",
            {
              staticClass:
                "sidebar-brand d-flex align-items-center justify-content-center",
              attrs: { href: "/Dashboard" }
            },
            [
              _c("div", { staticClass: "sidebar-brand-text mx-3" }, [
                _c("img", { attrs: { src: __webpack_require__(/*! ./logo.png */ "./resources/js/layouts/template/logo.png"), alt: "" } })
              ])
            ]
          ),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c("a", { staticClass: "nav-link", attrs: { href: "/Partner" } }, [
              _c("i", { staticClass: "fal fa-handshake" }),
              _vm._v(" "),
              _c("span", [_vm._v("Partner")])
            ])
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              { staticClass: "nav-link", attrs: { href: "/Listartikel" } },
              [
                _c("i", { staticClass: "fal fa-upload fa-2x" }),
                _vm._v(" "),
                _c("span", [_vm._v("Artikel")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              { staticClass: "nav-link", attrs: { href: "/Listpromosi" } },
              [
                _c("i", { staticClass: "fal fa-upload fa-2x" }),
                _vm._v(" "),
                _c("span", [_vm._v("Promotion")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              { staticClass: "nav-link", attrs: { href: "/Listtestimonial" } },
              [
                _c("i", { staticClass: "fal fa-upload fa-2x" }),
                _vm._v(" "),
                _c("span", [_vm._v("Testimonial")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              {
                staticClass: "nav-link collapsed",
                attrs: {
                  href: "#",
                  "data-toggle": "collapse",
                  "data-target": "#collapseUtilities",
                  "aria-expanded": "true",
                  "aria-controls": "collapseUtilities"
                }
              },
              [
                _c("i", { staticClass: "fal fa-file-chart-line" }),
                _vm._v(" "),
                _c("span", [_vm._v("Report")])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "collapse",
                attrs: {
                  id: "collapseUtilities",
                  "aria-labelledby": "headingUtilities",
                  "data-parent": "#accordionSidebar"
                }
              },
              [
                _c(
                  "div",
                  { staticClass: "bg-white py-2 collapse-inner rounded" },
                  [
                    _c(
                      "a",
                      {
                        staticClass: "collapse-item",
                        attrs: { href: "/Dashboard" }
                      },
                      [
                        _c("i", { staticClass: "fal fa-book mr-3" }),
                        _vm._v("Summary")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "collapse-item",
                        attrs: { href: "/Payment" }
                      },
                      [
                        _c("i", { staticClass: "fal fa-dollar-sign mr-3" }),
                        _vm._v("\n            Payment Billing")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "collapse-item",
                        attrs: { href: "/Listpartner" }
                      },
                      [
                        _c("i", { staticClass: "fal fa-users-class mr-3" }),
                        _vm._v("List of Partner")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "collapse-item",
                        attrs: { href: "/Listproduk" }
                      },
                      [
                        _c("i", {
                          staticClass: "fal fa-clipboard-list-check mr-3"
                        }),
                        _vm._v("List of Product")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      {
                        staticClass: "collapse-item",
                        attrs: { href: "/applog" }
                      },
                      [
                        _c("i", { staticClass: "fal fa-cog mr-3" }),
                        _vm._v("Application Log")
                      ]
                    )
                  ]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _c("li", { staticClass: "nav-item" }, [
            _c(
              "a",
              { staticClass: "nav-link", attrs: { href: "/profilecms" } },
              [
                _c("i", { staticClass: "fal fa-user-cog mr-3" }),
                _vm._v(" "),
                _c("span", [_vm._v("Account Setting")])
              ]
            )
          ]),
          _vm._v(" "),
          _c("hr", { staticClass: "sidebar-divider" }),
          _vm._v(" "),
          _c("div", { staticClass: "text-center d-none d-md-inline" }, [
            _c("button", {
              staticClass: "rounded-circle border-0",
              attrs: { id: "sidebarToggle" }
            })
          ])
        ]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "wrapper" } },
    [_c("Sidebar"), _vm._v(" "), _vm._m(0)],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "d-flex flex-column", attrs: { id: "content-wrapper" } },
      [
        _c("div", { attrs: { id: "content" } }, [
          _c("div", { staticClass: "container-fluid" }, [
            _c("main", { attrs: { id: "main" } }, [
              _c("div", { staticClass: "container" }, [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col mt-5" }, [
                    _c("h1", [_vm._v("Profile Anda")]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Selamat datang,Tegar")])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row text-center" }, [
                  _c("div", { staticClass: "col Profile" }, [
                    _c("img", {
                      staticClass: "rounded-circle",
                      attrs: { src: "/assets/img/lilo.png" }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row justify-content-center mt-3" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "media" }, [
                      _c("img", {
                        staticClass: "mr-3",
                        attrs: { src: "/assets/img/user.png", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "media-body" }, [
                        _c("div", { staticClass: "testimonial" }, [
                          _c("p", [_vm._v("Nama lengkap")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "overview" }, [
                            _vm._v("Lilo Digdya Pasha Santoso")
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row justify-content-center" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "media" }, [
                      _c("img", {
                        staticClass: "mr-3",
                        attrs: { src: "/assets/img/telp.png", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "media-body" }, [
                        _c("div", { staticClass: "testimonial" }, [
                          _c("p", [_vm._v("Nomor telepon")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "overview" }, [
                            _vm._v("0821 2312-2312")
                          ])
                        ])
                      ])
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row justify-content-center" }, [
                  _c("div", { staticClass: "col-sm-6" }, [
                    _c("div", { staticClass: "media" }, [
                      _c("img", {
                        staticClass: "mr-3",
                        attrs: { src: "/assets/img/mail.png", alt: "" }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "media-body" }, [
                        _c("div", { staticClass: "testimonial" }, [
                          _c("p", [_vm._v("Email")]),
                          _vm._v(" "),
                          _c("p", { staticClass: "overview" }, [
                            _vm._v("lilopsh@yahoo.com")
                          ])
                        ])
                      ])
                    ])
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("footer", { staticClass: "sticky-footer bg-white" }, [
          _c("div", { staticClass: "container my-auto" }, [
            _c("div", { staticClass: "copyright text-center my-auto" }, [
              _c("span", [_vm._v("Copyright © Your Website 2020")])
            ])
          ])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/layouts/template/Sidebar.vue":
/*!***************************************************!*\
  !*** ./resources/js/layouts/template/Sidebar.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Sidebar.vue?vue&type=template&id=5326452a& */ "./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/layouts/template/Sidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a&":
/*!**********************************************************************************!*\
  !*** ./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Sidebar.vue?vue&type=template&id=5326452a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/layouts/template/Sidebar.vue?vue&type=template&id=5326452a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Sidebar_vue_vue_type_template_id_5326452a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/layouts/template/logo.png":
/*!************************************************!*\
  !*** ./resources/js/layouts/template/logo.png ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/logo.png?d6f19043e1f3351bfdcef8717974578e";

/***/ }),

/***/ "./resources/js/views/cms/profile.vue":
/*!********************************************!*\
  !*** ./resources/js/views/cms/profile.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.vue?vue&type=template&id=5cfc2d34& */ "./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34&");
/* harmony import */ var _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.vue?vue&type=script&lang=js& */ "./resources/js/views/cms/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__["render"],
  _profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/cms/profile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/cms/profile.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/cms/profile.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/cms/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=template&id=5cfc2d34& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/cms/profile.vue?vue&type=template&id=5cfc2d34&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_5cfc2d34___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);