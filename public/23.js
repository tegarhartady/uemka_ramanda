(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[23],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikelpage.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Artikelpage.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constant_index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constant/index.js */ "./resources/js/constant/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      artikel: {}
    };
  },
  created: function created() {
    this.loadData();
  },
  methods: {
    loadData: function loadData(event) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(_constant_index_js__WEBPACK_IMPORTED_MODULE_2__["URL_API"] + "api/auth/artikel/ambildata", {
        slug: this.$route.params.slug
      }).then(function (response) {
        _this.artikel = response.data;
        console.log(_this.artikel);
      });
    },
    // moment : function(date) {
    //   return moment(date).format('MMMM Do YYYY, h:mm:ss a');
    // }
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col ml-5 mt-5" }, [
            _c("ul", { staticStyle: { "list-style": "none" } }, [
              _c("li", [_c("h2", [_vm._v(_vm._s(_vm.artikel.judul_artikel))])]),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _vm._m(3)
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-lg-12 text-center" }, [
        _c("img", {
          staticClass: "img-fluid",
          attrs: { src: "/assets/img/artikel.png", alt: "" }
        })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("p", [_vm._v("Ditulis oleh Lilo Pasha : Senin November 2020")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _c("p", [
        _vm._v(
          '\n                Section 1.10.32 of "de Finibus Bonorum et Malorum", written by\n                Cicero in 45 BC "Sed ut perspiciatis unde omnis iste natus\n                error sit voluptatem accusantium doloremque laudantium, totam\n                rem aperiam, eaque ipsa quae ab illo inventore veritatis et\n                quasi architecto beatae vitae dicta sunt explicabo. Nemo enim\n                ipsam voluptatem quia voluptas sit aspernatur aut odit aut\n                fugit, sed quia consequuntur magni dolores eos qui ratione\n                voluptatem sequi nesciunt. Neque porro quisquam est, qui\n                dolorem ipsum quia dolor sit amet, consectetur, adipisci\n                velit, sed quia non numquam eius modi tempora incidunt ut\n                labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad\n                minima veniam, quis nostrum exercitationem ullam corporis\n                suscipit laboriosam, nisi ut aliquid ex ea commodi\n                consequatur? Quis autem vel eum iure reprehenderit qui in ea\n                voluptate velit esse quam nihil molestiae consequatur, vel\n                illum qui dolorem eum fugiat quo voluptas nulla pariatur?"\n                1914 translation by H. Rackham "But I must explain to you how\n                all this mistaken idea of denouncing pleasure and praising\n                pain was born and I will give you a complete account of the\n                system, and expound the actual teachings of the great explorer\n                of the truth, the master-builder of human happiness. No one\n                rejects, dislikes, or avoids pleasure itself, because it is\n                pleasure, but because those who do not know how to pursue\n                pleasure rationally encounter consequences that are extremely\n                painful. Nor again is there anyone who loves or pursues or\n                desires to obtain pain of itself, because it is pain, but\n                because occasionally circumstances occur in which toil and\n                pain can procure him some great pleasure. To take a trivial\n                example, which of us ever undertakes laborious physical\n                exercise, except to obtain some advantage from it? But who has\n                any right to find fault with a man who chooses to enjoy a\n                pleasure that has no annoying consequences, or one who avoids\n                a pain that produces no resultant pleasure?" Section 1.10.33\n                of "de Finibus Bonorum et Malorum" "At vero eos et accusamus\n                et iusto odio dignissimos ducimus qui blanditiis praesentium\n                voluptatum deleniti atque corrupti quos dolores et quas\n                molestias excepturi sint occaecati cupiditate non provident,\n                similique sunt in culpa qui officia deserunt mollitia animi,\n                id est laborum et dolorum fuga. Et harum quidem rerum facilis\n                est et expedita distinctio. Nam libero tempore, cum soluta\n                nobis est eligendi optio cumque nihil impedit quo minus id\n                quod maxime placeat facere possimus, omnis voluptas assumenda\n                est, omnis dolor repellendus. Temporibus autem quibusdam et\n                aut officiis debitis aut rerum necessitatibus saepe eveniet ut\n                et voluptates repudiandae sint et molestiae non recusandae.\n                Itaque earum rerum hic tenetur a sapiente delectus, ut aut\n                reiciendis voluptatibus maiores alias consequatur aut\n                perferendis doloribus asperiores repellat." Section "At vero\n                eos et accusamus et iusto odio dignissimos ducimus qui\n                blanditiis praesentium voluptatum deleniti atque corrupti quos\n                dolores et quas molestias excepturi sint occaecati cupiditate\n                non provident, similique sunt in culpa qui officia deserunt\n                mollitia animi, id est laborum et dolorum fuga. Et harum\n                quidem rerum facilis est et expedita distinctio. Nam libero\n                tempore, cum soluta nobis est eligendi optio cumque nihil\n                impedit quo minus id quod maxime placeat facere possimus,\n                omnis voluptas assumenda est, omnis dolor repellendus.\n                Temporibus autem quibusdam et aut officiis debitis aut rerum\n                necessitatibus saepe eveniet ut et voluptates repudiandae sint\n                et molestiae non recusandae. Itaque earum rerum hic tenetur a\n                sapiente delectus, ut aut reiciendis voluptatibus maiores\n                alias consequatur aut perferendis doloribus asperiores\n                repellat." Section 1.10.33 "At vero eos et accusamus et iusto\n                odio dignissimos ducimus qui blanditiis praesentium voluptatum\n                deleniti atque corrupti quos dolores et quas molestias\n                excepturi sint occaecati cupiditate non provident, similique\n                sunt in culpa qui officia deserunt mollitia animi, id est\n                laborum et dolorum fuga. Et harum quidem rerum facilis est et\n                expedita distinctio. Nam libero tempore, cum soluta nobis est\n                eligendi optio cumque nihil impedit quo minus id quod maxime\n                placeat facere possimus, omnis voluptas assumenda est, omnis\n                dolor repellendus. Temporibus autem quibusdam et aut officiis\n                debitis aut rerum necessitatibus saepe eveniet ut et\n                voluptates repudiandae sint et molestiae non recusandae.\n                Itaque earum rerum hic tenetur a sapiente delectus, ut aut\n                reiciendis voluptatibus maiores alias consequatur aut\n                perferendis doloribus asperiores repellat."\n              '
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", { staticClass: "mt-4" }, [
      _c("p", [
        _vm._v(
          '\n                Section 1.10.32 of "de Finibus Bonorum et Malorum", written by\n                Cicero in 45 BC "Sed ut perspiciatis unde omnis iste natus\n                error sit voluptatem accusantium doloremque laudantium, totam\n                rem aperiam, eaque ipsa quae ab illo inventore veritatis et\n                quasi architecto beatae vitae dicta sunt explicabo. Nemo enim\n                ipsam voluptatem quia voluptas sit aspernatur aut odit aut\n                fugit, sed quia consequuntur magni dolores eos qui ratione\n                voluptatem sequi nesciunt. Neque porro quisquam est, qui\n                dolorem ipsum quia dolor sit amet, consectetur, adipisci\n                velit, sed quia non numquam eius modi tempora incidunt ut\n                labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad\n                minima veniam, quis nostrum exercitationem ullam corporis\n                suscipit laboriosam, nisi ut aliquid ex ea commodi\n                consequatur? Quis autem vel eum iure reprehenderit qui in ea\n                voluptate velit esse quam nihil molestiae consequatur, vel\n                illum qui dolorem eum fugiat quo voluptas nulla pariatur?"\n                1914 translation by H. Rackham "But I must explain to you how\n                all this mistaken idea of denouncing pleasure and praising\n                pain was born and I will give you a complete account of the\n                system, and expound the actual teachings of the great explorer\n                of the truth, the master-builder of human happiness. No one\n                rejects, dislikes, or avoids pleasure itself, because it is\n                pleasure, but because those who do not know how to pursue\n                pleasure rationally encounter consequences that are extremely\n                painful. Nor again is there anyone who loves or pursues or\n                desires to obtain pain of itself, because it is pain, but\n                because occasionally circumstances occur in which toil and\n                pain can procure him some great pleasure. To take a trivial\n                example, which of us ever undertakes laborious physical\n                exercise, except to obtain some advantage from it? But who has\n                any right to find fault with a man who chooses to enjoy a\n                pleasure that has no annoying consequences, or one who avoids\n                a pain that produces no resultant pleasure?" Section 1.10.33\n                of "de Finibus Bonorum et Malorum" "At vero eos et accusamus\n                et iusto odio dignissimos ducimus qui blanditiis praesentium\n                voluptatum deleniti atque corrupti quos dolores et quas\n                molestias excepturi sint occaecati cupiditate non provident,\n                similique sunt in culpa qui officia deserunt mollitia animi,\n                id est laborum et dolorum fuga. Et harum quidem rerum facilis\n                est et expedita distinctio. Nam libero tempore, cum soluta\n                nobis est eligendi optio cumque nihil impedit quo minus id\n                quod maxime placeat facere possimus, omnis voluptas assumenda\n                est, omnis dolor repellendus. Temporibus autem quibusdam et\n                aut officiis debitis aut rerum necessitatibus saepe eveniet ut\n                et voluptates repudiandae sint et molestiae non recusandae.\n                Itaque earum rerum hic tenetur a sapiente delectus, ut aut\n                reiciendis voluptatibus maiores alias consequatur aut\n                perferendis doloribus asperiores repellat." Section "At vero\n                eos et accusamus et iusto odio dignissimos ducimus qui\n                blanditiis praesentium voluptatum deleniti atque corrupti quos\n                dolores et quas molestias excepturi sint occaecati cupiditate\n                non provident, similique sunt in culpa qui officia deserunt\n                mollitia animi, id est laborum et dolorum fuga. Et harum\n                quidem rerum facilis est et expedita distinctio. Nam libero\n                tempore, cum soluta nobis est eligendi optio cumque nihil\n                impedit quo minus id quod maxime placeat facere possimus,\n                omnis voluptas assumenda est, omnis dolor repellendus.\n                Temporibus autem quibusdam et aut officiis debitis aut rerum\n                necessitatibus saepe eveniet ut et voluptates repudiandae sint\n                et molestiae non recusandae. Itaque earum rerum hic tenetur a\n                sapiente delectus, ut aut reiciendis voluptatibus maiores\n                alias consequatur aut perferendis doloribus asperiores\n                repellat." Section 1.10.33 "At vero eos et accusamus et iusto\n                odio dignissimos ducimus qui blanditiis praesentium voluptatum\n                deleniti atque corrupti quos dolores et quas molestias\n                excepturi sint occaecati cupiditate non provident, similique\n                sunt in culpa qui officia deserunt mollitia animi, id est\n                laborum et dolorum fuga. Et harum quidem rerum facilis est et\n                expedita distinctio. Nam libero tempore, cum soluta nobis est\n                eligendi optio cumque nihil impedit quo minus id quod maxime\n                placeat facere possimus, omnis voluptas assumenda est, omnis\n                dolor repellendus. Temporibus autem quibusdam et aut officiis\n                debitis aut rerum necessitatibus saepe eveniet ut et\n                voluptates repudiandae sint et molestiae non recusandae.\n                Itaque earum rerum hic tenetur a sapiente delectus, ut aut\n                reiciendis voluptatibus maiores alias consequatur aut\n                perferendis doloribus asperiores repellat."\n              '
        )
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/constant/index.js":
/*!****************************************!*\
  !*** ./resources/js/constant/index.js ***!
  \****************************************/
/*! exports provided: URL_API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_API", function() { return URL_API; });
var URL_API = 'https://uemka.sitama.co.id/';

/***/ }),

/***/ "./resources/js/views/Artikelpage.vue":
/*!********************************************!*\
  !*** ./resources/js/views/Artikelpage.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Artikelpage.vue?vue&type=template&id=53fcbe28& */ "./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28&");
/* harmony import */ var _Artikelpage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Artikelpage.vue?vue&type=script&lang=js& */ "./resources/js/views/Artikelpage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Artikelpage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Artikelpage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Artikelpage.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/Artikelpage.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikelpage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Artikelpage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikelpage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikelpage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Artikelpage.vue?vue&type=template&id=53fcbe28& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Artikelpage.vue?vue&type=template&id=53fcbe28&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Artikelpage_vue_vue_type_template_id_53fcbe28___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);