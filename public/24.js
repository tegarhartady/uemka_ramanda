(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Promosi.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constant_index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constant/index.js */ "./resources/js/constant/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      promosi: {}
    };
  },
  created: function created() {
    this.loadData();
  },
  methods: {
    loadData: function loadData(event) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(_constant_index_js__WEBPACK_IMPORTED_MODULE_2__["URL_API"] + "api/auth/promosi/ambildata", {
        slug: this.$route.params.slug
      }).then(function (response) {
        _this.promosi = response.data;
        console.log(_this.promosi);
      });
    },
    // moment : function(date) {
    //   return moment(date).format('MMMM Do YYYY, h:mm:ss a');
    // }
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=template&id=46710956&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Promosi.vue?vue&type=template&id=46710956& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("section", { attrs: { id: "headpromosi" } }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "row text-center mb-5" }, [
        _c("div", { staticClass: "col" }, [
          _c("h2", [
            _vm._v(
              "\n          " +
                _vm._s(_vm.promosi.judul_promosi) +
                "\n          \n        "
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row text-center mt-5" }, [
        _c("div", { staticClass: "col" }, [
          _c("img", {
            staticClass: "img-fluid",
            attrs: {
              src:
                "https://file.sitama.co.id/storage/uemka/uemka/Promosi" +
                _vm.promosi.image_promosi,
              alt: ""
            }
          })
        ])
      ])
    ]),
    _vm._v(" "),
    _c("section", { attrs: { id: "syarat" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row kotak" }, [
          _c("div", { staticClass: "col" }, [
            _c("div", {
              domProps: { innerHTML: _vm._s(_vm.promosi.isi_promosi) }
            })
          ])
        ])
      ])
    ]),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _vm._m(3)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row text-center mb-5" }, [
      _c("div", { staticClass: "col" }, [_c("h1", [_vm._v("Promosi")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "share" } }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col text-center" }, [
          _c("h4", [_vm._v("Bagikan")]),
          _vm._v(" "),
          _c("img", {
            staticClass: "rounded-circle mr-3",
            attrs: { src: "assets/img/share/m.png", alt: "" }
          }),
          _vm._v(" "),
          _c("img", {
            staticClass: "rounded-circle mr-3",
            attrs: { src: "assets/img/share/c.png", alt: "" }
          }),
          _vm._v(" "),
          _c("img", {
            staticClass: "rounded-circle mr-3",
            attrs: { src: "assets/img/share/iya.png", alt: "" }
          })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "ASKME" } }, [
      _c("div", { staticClass: "jumbotron jumbotron-fluid" }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body" }, [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                  Memiliki pertanyaan seputar pembiayaan?\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("a", { attrs: { href: "#" } }, [
                    _vm._v("Chat sekarang\n                  "),
                    _c("i", {
                      staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                    })
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body" }, [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v("Ingin mengajukan pembiayaan?")
                  ]),
                  _vm._v(" "),
                  _c("a", { attrs: { href: "#" } }, [
                    _vm._v("AJUKAN SEKARANG\n                  "),
                    _c("i", {
                      staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                    })
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-lg-4 col-sm-4" }, [
              _c("div", { staticClass: "card" }, [
                _c("div", { staticClass: "card-body" }, [
                  _c("p", { staticClass: "card-text" }, [
                    _vm._v(
                      "\n                  Temukan cabang atau gerai BFI terdekat\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("a", { attrs: { href: "#" } }, [
                    _vm._v("LIHAT LOKASI\n                  "),
                    _c("i", {
                      staticClass: "fas fa-long-arrow-alt-left fa-rotate-180"
                    })
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { attrs: { id: "promotion" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col text-center mb-5" }, [
            _c("h1", [_vm._v("Promosi")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col" }, [
          _c(
            "div",
            {
              staticClass: "carousel slide",
              attrs: { id: "carouselExampleControls", "data-ride": "carousel" }
            },
            [
              _c("div", { staticClass: "carousel-inner" }, [
                _c("div", { staticClass: "carousel-item active" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "First slide"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "carousel-item" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "Second slide"
                    }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "carousel-item" }, [
                  _c("img", {
                    staticClass: "d-block w-100",
                    attrs: {
                      src: "/assets/img/Project Cover.png",
                      alt: "Third slide"
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "carousel-control-prev",
                  attrs: {
                    href: "#carouselExampleControls",
                    role: "button",
                    "data-slide": "prev"
                  }
                },
                [
                  _c("span", {
                    staticClass: "carousel-control-prev-icon",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Previous")])
                ]
              ),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "carousel-control-next",
                  attrs: {
                    href: "#carouselExampleControls",
                    role: "button",
                    "data-slide": "next"
                  }
                },
                [
                  _c("span", {
                    staticClass: "carousel-control-next-icon",
                    attrs: { "aria-hidden": "true" }
                  }),
                  _vm._v(" "),
                  _c("span", { staticClass: "sr-only" }, [_vm._v("Next")])
                ]
              )
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/constant/index.js":
/*!****************************************!*\
  !*** ./resources/js/constant/index.js ***!
  \****************************************/
/*! exports provided: URL_API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_API", function() { return URL_API; });
var URL_API = 'https://uemka.sitama.co.id/';

/***/ }),

/***/ "./resources/js/views/Promosi.vue":
/*!****************************************!*\
  !*** ./resources/js/views/Promosi.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Promosi.vue?vue&type=template&id=46710956& */ "./resources/js/views/Promosi.vue?vue&type=template&id=46710956&");
/* harmony import */ var _Promosi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Promosi.vue?vue&type=script&lang=js& */ "./resources/js/views/Promosi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Promosi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Promosi.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Promosi.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/views/Promosi.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Promosi.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Promosi.vue?vue&type=template&id=46710956&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/Promosi.vue?vue&type=template&id=46710956& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Promosi.vue?vue&type=template&id=46710956& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Promosi.vue?vue&type=template&id=46710956&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Promosi_vue_vue_type_template_id_46710956___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);