(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[26],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/bprdanksp.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constant_index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constant/index.js */ "./resources/js/constant/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      testimoni: [],
      kospin: [],
      bpr: []
    };
  },
  created: function created() {
    this.loadDatatesti(), this.loadDatakospin(), this.loadDatabpr();
  },
  methods: {
    linkWhatsapp: function linkWhatsapp() {
      location.href = "https://api.whatsapp.com/send?phone=6288290518571&text=test%20Whatsapp%20Services";
    },
    allPromosi: function allPromosi() {
      location.href = "Promosipage";
    },
    loadDatatesti: function loadDatatesti(event) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(_constant_index_js__WEBPACK_IMPORTED_MODULE_1__["URL_API"] + "api/auth/testimoni/limit").then(function (response) {
        _this.testimoni = response.data;
      });
    },
    loadDatakospin: function loadDatakospin(event) {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(_constant_index_js__WEBPACK_IMPORTED_MODULE_1__["URL_API"] + "api/auth/partner/jenis/all/kospin").then(function (response) {
        _this2.kospin = response.data;
      });
    },
    loadDatabpr: function loadDatabpr(event) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(_constant_index_js__WEBPACK_IMPORTED_MODULE_1__["URL_API"] + "api/auth/partner/jenis/all/bpr").then(function (response) {
        _this3.bpr = response.data;
        console.log(_this3.bpr);
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "section",
      {
        staticClass: "whyme",
        staticStyle: { "background-color": "#fdc134" },
        attrs: { id: "whyme" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _vm._m(0),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "row" },
            _vm._l(_vm.kospin, function(kos) {
              return _c(
                "div",
                { key: kos.idpartner, staticClass: "col-lg-4 mb-4" },
                [
                  _c("div", { staticClass: "card" }, [
                    _c("div", { staticClass: "card-body text-center" }, [
                      _c("div", { staticClass: "img" }, [
                        _c("img", {
                          attrs: {
                            src:
                              "https://file.sitama.co.id/storage/uemka/uemka/Partner" +
                              kos.image_partner,
                            alt: ""
                          }
                        })
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "content-img" }, [
                        _c("ul", { staticStyle: { "list-style": "none" } }, [
                          _c("div", {
                            domProps: {
                              innerHTML: _vm._s(kos.visimisi_partner)
                            }
                          })
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "button",
                        { staticClass: "btn btn-dark" },
                        [
                          _c(
                            "router-link",
                            {
                              staticStyle: { color: "#FDC134" },
                              attrs: {
                                tag: "a",
                                to: "/kspperusahaan/" + kos.idpartner
                              }
                            },
                            [
                              _vm._v(
                                "\n                    Lihat Detail\n                  "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ])
                  ])
                ]
              )
            }),
            0
          ),
          _vm._v(" "),
          _vm._m(1)
        ])
      ]
    ),
    _vm._v(" "),
    _c(
      "section",
      {
        staticClass: "whyme",
        staticStyle: { "background-color": "#213b52 !important" },
        attrs: { id: "whyme" }
      },
      [
        _c("div", { staticClass: "container" }, [
          _vm._m(2),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "row" },
            _vm._l(_vm.bpr, function(bp) {
              return _c("div", { key: bp.idpartner, staticClass: "col-lg-4" }, [
                _c("div", { staticClass: "card" }, [
                  _c("div", { staticClass: "card-body text-center" }, [
                    _c("div", { staticClass: "img" }, [
                      _c("img", {
                        attrs: {
                          img: "",
                          src:
                            "https://file.sitama.co.id/storage/uemka/uemka/Partner" +
                            bp.image_partner,
                          alt: ""
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "content-img" }, [
                      _c("ul", { staticStyle: { "list-style": "none" } }, [
                        _c("div", {
                          domProps: { innerHTML: _vm._s(bp.visimisi_partner) }
                        })
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      { staticClass: "btn btn-dark" },
                      [
                        _c(
                          "router-link",
                          {
                            staticStyle: { color: "#FDC134" },
                            attrs: {
                              tag: "a",
                              to: "/kspperusahaan/" + bp.idpartner
                            }
                          },
                          [
                            _vm._v(
                              "\n                    Lihat Detail\n                  "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ])
                ])
              ])
            }),
            0
          ),
          _vm._v(" "),
          _vm._m(3)
        ])
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container" }, [
      _c(
        "div",
        { staticClass: "section-title", attrs: { "data-aos": "fade-up" } },
        [
          _c("h1", { staticStyle: { color: "#213b52" } }, [
            _c("strong", [_vm._v("Koperasi Simpan Pinjam")])
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center koperasi" }, [
      _c("button", { staticClass: "btn btn-outline-dark" }, [
        _c("a", { attrs: { href: "/Detailkspdankpr" } }, [
          _vm._v("\n              Lihat lainnya\n              "),
          _c("i", { staticClass: "fas fa-long-arrow-alt-left fa-rotate-180" })
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container" }, [
      _c(
        "div",
        { staticClass: "section-title", attrs: { "data-aos": "fade-up" } },
        [
          _c("h1", { staticStyle: { color: "#ffffff" } }, [
            _c("strong", [_vm._v("Bank Perkreditan Rakyat")])
          ])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center koperasi1" }, [
      _c("button", { staticClass: "btn btn-outline-light" }, [
        _vm._v("\n            Lihat lainnya\n            "),
        _c("i", { staticClass: "fas fa-long-arrow-alt-left fa-rotate-180" })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/constant/index.js":
/*!****************************************!*\
  !*** ./resources/js/constant/index.js ***!
  \****************************************/
/*! exports provided: URL_API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_API", function() { return URL_API; });
var URL_API = 'https://uemka.sitama.co.id/';

/***/ }),

/***/ "./resources/js/views/bprdanksp.vue":
/*!******************************************!*\
  !*** ./resources/js/views/bprdanksp.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bprdanksp.vue?vue&type=template&id=0832b64c& */ "./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&");
/* harmony import */ var _bprdanksp_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bprdanksp.vue?vue&type=script&lang=js& */ "./resources/js/views/bprdanksp.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _bprdanksp_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/bprdanksp.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/bprdanksp.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/views/bprdanksp.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./bprdanksp.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./bprdanksp.vue?vue&type=template&id=0832b64c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/bprdanksp.vue?vue&type=template&id=0832b64c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_bprdanksp_vue_vue_type_template_id_0832b64c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);