(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constant_index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constant/index.js */ "./resources/js/constant/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      kospin: {}
    };
  },
  created: function created() {
    this.loadData();
  },
  methods: {
    loadData: function loadData(event) {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post(_constant_index_js__WEBPACK_IMPORTED_MODULE_2__["URL_API"] + "api/auth/partner/ambildata/kospin", {
        idpartner: this.$route.params.idpartner
      }).then(function (response) {
        _this.kospin = response.data;
        console.log(_this.kospin);
      });
    },
    // moment : function(date) {
    //   return moment(date).format('MMMM Do YYYY, h:mm:ss a');
    // }
    moment: function moment(date) {
      return moment__WEBPACK_IMPORTED_MODULE_1___default()(date);
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("main", { attrs: { id: "main" } }, [
      _c("div", { staticClass: "container ksp" }, [
        _vm._m(0),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "col-lg-6" }, [
            _c("div", { staticClass: "deskripsi" }, [
              _c("ul", { staticStyle: { "list-style": "none" } }, [
                _c("h5", [_vm._v("Koperasi")]),
                _vm._v(" "),
                _c("p", { staticClass: "h2" }, [
                  _vm._v(_vm._s(_vm.kospin.nama_partner))
                ]),
                _vm._v(" "),
                _c("li", { staticClass: "parent" }, [_vm._v("Alamat")]),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _c("li", { staticClass: "parent mt-3" }, [
                  _vm._v("Nomor Telepon")
                ]),
                _vm._v(" "),
                _c("li", [_vm._v("0813 1114-4407")]),
                _vm._v(" "),
                _c("li", { staticClass: "parent mt-3" }, [_vm._v("Website")]),
                _vm._v(" "),
                _c("li", [_vm._v("www.surya-kencana.com")])
              ]),
              _vm._v(" "),
              _c("br")
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(3)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col text-center" }, [
        _c("button", { staticClass: "btn btn-warning " }, [
          _vm._v("Profil Perusahaan")
        ]),
        _vm._v(" "),
        _c("button", { staticClass: "btn btn ksp-btn" }, [
          _vm._v("Profil Perusahaan")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-lg-6" }, [
      _c("img", { attrs: { src: "/assets/img/kpkn.png", alt: "" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("li", [
      _vm._v("RUKO GOLDEN MADRID I BLOK D NO.25 "),
      _c("br"),
      _vm._v(
        "\n                            Jl. Letnan Sutopo, Rw. Mekar Jaya, Kec. Serpong, Kota "
      ),
      _c("br"),
      _vm._v(" Tangerang Selatan, Banten\n                            15111")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", { staticClass: "desk" }, [
      _c("div", { staticClass: "container text-left" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col" }, [
            _c("h1", [_vm._v("Product")]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-1" }, [
                _c("hr", { staticStyle: { border: "1px solid" } })
              ])
            ]),
            _vm._v(" "),
            _c("ul", { staticStyle: { "list-style": "none" } }, [
              _c("li", [
                _vm._v(
                  "1) Pinjaman - Agunan BPKB\n                            "
                ),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v(
                    "\n                                Kredit untuk semua kebetuhan anda, diperuntukkan untuk segala kalangan, baik untuk\n                                perorangan, usaha kecil/m "
                  ),
                  _c("br"),
                  _vm._v(
                    "\n                                maupun perusahaan besar "
                  ),
                  _c("br"),
                  _vm._v(" "),
                  _c("br"),
                  _vm._v(
                    "\n                                Bunga:1,5%\n                                "
                  ),
                  _c("br"),
                  _vm._v(" "),
                  _c("br"),
                  _vm._v(" Max Tenor: 5 Tahun\n                            ")
                ])
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  "2) Pinjaman - Agunan SHM\n                            "
                ),
                _c("br"),
                _vm._v(" "),
                _c("p", { staticClass: "mt-4" }, [
                  _vm._v(
                    "\n                                Kredit untuk semua kebetuhan anda, diperuntukkan untuk segala kalangan, baik untuk\n                                perorangan, usaha kecil/m "
                  ),
                  _c("br"),
                  _vm._v(
                    "\n                                maupun perusahaan besar "
                  ),
                  _c("br"),
                  _vm._v(" "),
                  _c("br"),
                  _vm._v(
                    "\n                                Bunga:1,25%\n                                "
                  ),
                  _c("br"),
                  _vm._v(" "),
                  _c("br"),
                  _vm._v(" Max Tenor: 15 Tahun\n                            ")
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col mt-5" }, [
            _c("h1", [_vm._v("Persyaratan")]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-1" }, [
                _c("hr", { staticStyle: { border: "1px solid" } })
              ])
            ]),
            _vm._v(" "),
            _c("ul", { staticStyle: { "list-style": "none" } }, [
              _c("li", { staticClass: "mb-2" }, [_vm._v("Persyaratan")]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  "- Warga Negara Indonesia, baik perorangan maupun perusahaan"
                )
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  " - Usia telah 21 tahun dan maksimal 58 tahun saat pinjaman berakhir"
                )
              ]),
              _vm._v(" "),
              _c("li", [_vm._v("- Foto Copy Kartu Keluarga (KK)")]),
              _vm._v(" "),
              _c("li", [
                _vm._v(
                  "- Foto Copy Identitas Diri yang masih berlaku (KTP/Paspor/SIM)"
                )
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("- Foto Copy surat Nikah (Untuk yang sudah menikah")
              ]),
              _vm._v(" "),
              _c("li", [
                _vm._v("- Foto Copy surat Nikah (Untuk yang sudah menikah")
              ]),
              _vm._v(" "),
              _c("li", [_vm._v("- Pas Foto Suami dan Istri")]),
              _vm._v(" "),
              _c("li", [_vm._v("- NPWP Pribadi")])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-12 text-center" }, [
            _c("button", { staticClass: "btn btn-warning" }, [
              _vm._v("Dapatkan Penawaran")
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/constant/index.js":
/*!****************************************!*\
  !*** ./resources/js/constant/index.js ***!
  \****************************************/
/*! exports provided: URL_API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_API", function() { return URL_API; });
var URL_API = 'https://uemka.sitama.co.id/';

/***/ }),

/***/ "./resources/js/views/kspperusahaan.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/kspperusahaan.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./kspperusahaan.vue?vue&type=template&id=0fc8c8c5& */ "./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5&");
/* harmony import */ var _kspperusahaan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./kspperusahaan.vue?vue&type=script&lang=js& */ "./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _kspperusahaan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/kspperusahaan.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_kspperusahaan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./kspperusahaan.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/kspperusahaan.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_kspperusahaan_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./kspperusahaan.vue?vue&type=template&id=0fc8c8c5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/kspperusahaan.vue?vue&type=template&id=0fc8c8c5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_kspperusahaan_vue_vue_type_template_id_0fc8c8c5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);