(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("section", [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "col text-center" }, [
          _c("h1", [_vm._v("Syarat dan Ketentuan")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col text-left" }, [
            _c("p", { staticClass: "mt-5" }, [
              _vm._v("\n          Selamat datang di www.uemka.com\n          "),
              _c("br"),
              _vm._v(" "),
              _c("br"),
              _vm._v(
                "\n          Disarankan sebelum mengakses Situs ini lebih jauh, Anda terlebih\n          dahulu membaca dan memahami syarat dan ketentuan yang berlaku.\n          Syarat dan ketentuan berikut adalah ketentuan dalam pengunjungan\n          Situs, isi dan/atau konten, layanan, serta fitur lainnya yang ada di\n          dalam Situs. Dengan mengakses atau menggunakan Situs atau aplikasi\n          lainnya yang disediakan oleh atau dalam Situs, berarti Anda telah\n          memahami dan menyetujui serta terikat dan tunduk dengan segala\n          syarat dan ketentuan yang berlaku di Situs ini.\n        "
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h3", [_vm._v("DEFINISI")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Setiap kata atau istilah berikut yang digunakan di dalam syarat\n              & ketentuan ini memiliki arti seperti berikut di bawah, kecuali\n              jika kata atau istilah yang bersangkutan di dalam pemakaiannya\n              dengan tegas menentukan lain\n            "
                  )
                ]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _vm._v(
                      '\n                "Kami", berarti PT Solusi Integrasi Pratama selaku pemilik dan\n                pengelola Situs www.uemka.com\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Anda", berarti tiap orang yang mengakses dan menggunakan\n                layanan dan jasa yang disediakan oleh Kami.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Layanan", berarti tiap dan keseluruhan jasa serta informasi\n                yang ada pada Situs www.uemka.com, dan tidak terbatas pada\n                informasi yang disediakan, layanan web, dan dukungan data\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Pengguna", berarti tiap orang yang mengakses dan menggunakan\n                layanan dan jasa yang disediakan oleh Kami, termasuk\n                diantaranya Pengguna Belum Terdaftar dan Pengguna Terdaftar.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Pengguna Belum Terdaftar", berarti tiap orang yang mengakses\n                Situs Kami dan belum melakukan registrasi.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Pengguna Terdaftar", berarti tiap orang yang mengakses dan\n                menggunakan layanan dan jasa yang disediakan oleh Kami, serta\n                telah melakukan registrasi dan memiliki akun pada Situs Kami.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Pihak Ketiga", berarti pihak lainnya, termasuk namun tidak\n                terbatas seperti Lembaga keuangan dan jasa layanan lainnya\n                yang menyediakan layanannya dalam Situs.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Informasi Pribadi", berarti tiap dan seluruh data pribadi\n                yang diberikan oleh Pengguna di Situs Kami, termasuk namun\n                tidak terbatas pada nama lengkap, tempat dan tanggal lahir,\n                jenis kelamin, alamat, nomor identitas, informasi debitur,\n                surat nikah, bukti kepemilikan agunan, KTP, NPWP, lokasi\n                pengguna, kontak pengguna, serta dokumen dan data pendukung\n                lainnya sebagaimana diminta pada ringkasan pendaftaran akun\n                serta pada ringkasan aplikasi pengajuan.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      '\n                "Konten", berarti teks, data, informasi, angka, gambar,\n                grafik, foto, audio, video, nama pengguna, informasi,\n                aplikasi, tautan, komentar, peringkat, desain, atau materi\n                lainnya yang ditampilkan pada Situs.\n              '
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [_vm._v("“Situs”, berarti www.uemka.com")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("LAYANAN DAN/ATAU JASA")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Kami memberikan informasi mengenai produk-produk finansial\n              dan/atau perbankan, asuransi, telekomunikasi yang disediakan\n              oleh Pihak Ketiga. Layanan ini memungkinkan Anda untuk dapat\n              melihat, menelaah, membaca, serta membandingkan informasi\n              tersebut.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _vm._v(
                      "\n                Informasi yang terdapat dalam Situs Kami ditampilkan sesuai\n                keadaan kenyataan untuk tujuan informasi umum. Kami berusaha\n                untuk selalu menyediakan dan menampilkan informasi yang\n                terbaru dan akurat, namun Kami tidak menjamin bahwa segala\n                informasi sesuai dengan ketepatan waktu atau relevansi dengan\n                kebutuhan Anda.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Diharapkan Anda tidak menganggap atau berasumsi bahwa Situs\n                ini dapat dijadikan sebagai saran keuangan dan/atau finansial,\n                atau rekomendasi atas produk serta layanan yang ditampilkan.\n                Informasi di Situs ini disediakan untuk membantu Anda dalam\n                memilih produk atau layanan yang sesuai dengan kebutuhan Anda.\n                Anda sepenuhnya bertanggung jawab atas keputusan terkait\n                pemilihan produk dan layanan, atau dalam menandatangani\n                kontrak berkaitan dengan sebuah produk atau layanan. Dengan\n                memberikan Kami rincian tentang diri pribadi Anda, dianggap\n                Anda mengajukan diri untuk mendapatkan suatu produk atau\n                layanan tertentu.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Layanan yang Kami berikan tidak bersifat memaksa atau\n                mengikat, serta tidak mengharuskan Anda untuk mengikuti\n                informasi yang tersedia. Tidak ada situasi atau keadaan apapun\n                yang dapat membuat Kami dikenakan tanggung jawab atas\n                kemungkinan kerugian yang Anda alami karena pengambilan\n                keputusan yang dilakukan oleh Anda sendiri terkait tindakan\n                atas informasi produk yang kami sampaikan di Situs.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Kami memiliki hak untuk kapan saja menampilkan, mengubah,\n                menghapus, menghilangkan, atau menambahkan segala konten yang\n                ditampilkan dalam Situs ini.\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("PENGGUNAAN LAYANAN DAN JASA")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Dengan Anda melanjutkan penggunaan atau pengaksesan Situs ini,\n              berarti Anda telah menyatakan serta menjamin kepada Kami bahwa:\n            "
                  )
                ]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", { staticClass: "mb-1" }, [
                    _vm._v(
                      "\n                Anda hanya diperkenankan untuk mengakses dan/atau menggunakan\n                Situs ini untuk keperluan pribadi dan non-komersil, yang\n                berarti bahwa Situs ini hanya boleh diakses dan digunakan\n                secara langsung oleh individu atau bisnis yang sedang mencari\n                produk atau layanan untuk individu atau bisnis tersebut. Akses\n                dan penggunaan Situs diluar dari keperluan pribadi atau\n                non-komersil dilarang keras, dan melanggar Syarat dan\n                Ketentuan ini.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Anda tidak diperkenankan menggunakan Situs dalam hal sebagai\n                berikut :\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Untuk menyakiti, menyiksa, mempermalukan, memfitnah,\n                mencemarkan nama baik, mengancam, mengintimidasi atau\n                mengganggu orang atau bisnis lain, atau apapun yang melanggar\n                privasi atau yang Kami anggap cabul, menghina, penuh\n                kebencian, tidak senonoh, tidak patut, tidak pantas, tidak\n                dapat diterima, mendiskriminasikan atau merusak.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Dalam cara yang melawan hukum, tindakan penipuan atau tindakan\n                komersil.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Melanggar atau menyalahi hak orang lain, termasuk tanpa\n                kecuali : hak paten, merek dagang, hak cipta, rahasia dagang,\n                publisitas, dan hak milik lainnya.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Untuk membuat, memeriksa, memperbarui, mengubah atau\n                memperbaiki database, rekaman atau direktori Anda ataupun\n                orang lain.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Mengunakan kode komputer otomatis, proses, program, robot, net\n                crawler, spider, pemrosesan data, trawling atau kode komputer,\n                proses, program atau sistem screen scraping alternatif.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                Melanggar Syarat dan Ketentuan, atau petunjuk lainnya yang ada\n                pada Situs.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Kami tidak bertanggung jawab atas kehilangan akibat kegagalan\n                mengakses Situs, dan metode penggunaan Situs yang diluar\n                kendali Kami.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Kami tidak bertanggung jawab atau dapat dipersalahkan atas\n                kehilangan atau kerusakan yang diluar perkiraan saat Anda\n                mengakses atau menggunakan Situs ini. Ini termasuk kehilangan\n                penghematan yang diharapkan, kehilangan bisnis atau kesempatan\n                bisnis, kehilangan pemasukan atau keuntungan, atau kehilangan\n                atau kerusakan apapun yang Anda harus alami akibat penggunaan\n                Situs ini.\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Pengajuan Jasa/Layanan")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("ul", [
                  _c("li", { staticClass: "mb-1" }, [
                    _vm._v(
                      "\n                Anda bisa mengajukan diri untuk mendapatkan informasi dan\n                penawaran dari berbagai produk dan layanan seperti yang\n                ditampilkan di Situs ini. Produk dan layanan tersebut\n                disediakan oleh Pihak Ketiga. Anda sepenuhnya bertanggung\n                jawab atas pilihan produk atau layanan, dan Kami tidak\n                bertanggung jawab atau dapat dipersalahkan atas kehilangan\n                atau kerusakan yang mungkin Anda derita akibat produk atau\n                layanan yang Anda dapatkan saat menggunakan Situs ini, atau\n                atas tindakan, penghapusan atau kesalahan Pihak Ketiga yang\n                disebut di produk atau layanan itu.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Diingatkan kembali bahwa data dan deskripsi produk dan jasa\n                pada Situs ini mungkin tidak mewakili seluruh deskripsi dari\n                semua pilihan dan persyaratan produk dan layanan tersebut.\n                Anda perlu berhati-hati dan cermat untuk melihat semua pilihan\n                dan syarat dan kondisi dari setiap produk atau jasa sebelum\n                melakukan pengajuan.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Jika Anda mengajukan permohonan dan mendapatkan produk atau\n                layanan, Anda akan masuk ke dalam kontak dengan Pihak Ketiga\n                sebagai penyedia produk atau layanan, yang memiliki syarat dan\n                ketentuan sendiri. Anda bertanggung jawab untuk memastikan\n                bahwa Anda setuju dengan syarat dan kondisi sebelum memasuki\n                kontrak untuk mendapatkan produk atau layanan dimaksud. Kami\n                tidak bertanggung jawab atau disalahkan atas segala kerugian\n                atau kerusakan yang mungkin Anda derita akibat syarat dan\n                ketentuan yang berlaku di setiap kontrak antara Anda dengan\n                Pihak Ketiga, termasuk yang berhubungan tindakan, kelalaian\n                atau kesalahan dari Pihak Ketiga.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Anda memberikan izin pada Kami untuk memproses data-data Anda\n                kepada Pihak Ketiga, serta memberi kuasa kepada Pihak Ketiga\n                untuk memeriksa semua informasi yang diberikan dengan cara\n                bagaimanapun, menghubungi Anda untuk pemasaran dari produk\n                maupun layanan yang disediakan oleh Pihak Ketiga\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Anda memberikan izin pada Kami untuk memproses data Anda untuk\n                meningkatkan layanan Kami dalam bentuk apapun, termasuk tapi\n                tidak terbatas untuk keperluan pemasaran dan peningkatan\n                layanan Kami.\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Keputusan Finansial")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Layanan Kami menyediakan bagi Anda suatu tempat untuk melihat\n              suatu informasi mengenai jasa Pihak Ketiga seperti institusi\n              finansial, penyedia kartu kredit, institusi pinjaman, dan\n              asuransi. Anda wajib membuat pilihan dan keputusan sendiri\n              berdasarkan pertimbangan Anda sehubungan dengan penyedia jasa\n              Pihak Ketiga. Kami tidak memberikan validasi atau melakukan\n              pengesahan untuk penggunaan Pihak Ketiga oleh Anda yang\n              semata-mata merupakan keputusan anda sepenuhnya.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Anda dengan ini menerima dan menyetujui bahwa Anda akan\n              berhubungan langsung dengan Pihak Ketiga yang Anda tunjuk\n              dan/atau pilih dan oleh karenanya Pihak Ketiga tersebut\n              bertanggung jawab secara penuh atas jasa yang diberikan kepada\n              Anda. Kami tidak bertanggung jawab atas kerugian sehubungan\n              dengan atau yang muncul dari atau sehubungan dengan pilihan Anda\n              dan Kami menghimbau agar Anda mendapatkan advis sebelum\n              mengambil keputusan.\n            "
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Aturan Pengguna")]),
                _vm._v(" "),
                _c("br"),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Selain menampilkan informasi produk-produk finansial dan/atau\n              keuangan, asuransi, dan telekomunikasi, Kami juga menyediakan\n              keanggotaan bagi Anda selaku Pengguna. Anda dapat memeriksa\n              riwayat keuangan Anda dan juga menemukan produk yang sesuai\n              dengan profil Anda, serta juga dapat berpartisipasi di Forum\n              yang juga Kami sediakan.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _vm._v(
                      "\n                Sebelum menjadi Pengguna Terdaftar, Anda diwajibkan membuat\n                akun untuk diri Anda. Proses pembuatan akun dapat dilakukan\n                secara online melalui Situs.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Sebelum menjadi Pengguna Terdaftar, Anda diwajibkan membaca,\n                memahami, dan mematuhi Syarat dan Ketentuan serta Kebijakan\n                Privasi yang berlaku di Situs.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Anda diharuskan memberi data Informasi Pribadi yang sebenarnya\n                dan tidak memberikan informasi menyimpang dan/atau informasi\n                yang tidak relevan dalam melakukan proses registrasi menjadi\n                Pengguna Terdaftar. Selain itu Anda juga diharuskan memberi\n                kontak detail yang benar dan valid.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Kami berhak untuk tidak memproses registrasi Pengguna yang\n                tidak memenuhi persyaratan ataupun tidak memberikan informasi\n                yang benar dan valid, dan Kami berhak juga mengeluarkan\n                Pengguna Terdaftar dari keanggotaan jika di kemudian hari\n                ditemukan hal-hal yang melanggar ketentuan dari Kami.\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Pengguna yang telah sukses melakukan proses registrasi, dan\n                menjadi Pengguna Terdaftar, dapat melakukan akses keanggotaan\n                kapan pun melalui Situs\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                Semua Pengguna Terdaftar wajib mematuhi Syarat dan Ketentuan\n                yang berlaku di Situs ini, dan tidak diperkenankan melakukan\n                tindakan-tindakan seperti tertuang dalam poin Penggunaan\n                Layanan dan Jasa\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("TIDAK ADA JAMINAN")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              ANDA DENGAN INI MENERIMA DAN MENYETUJUI BAHWA, SEPANJANG\n              PERATURAN YANG BERLAKU MEMPERBOLEHKAN:\n            "
                  )
                ]),
                _vm._v(" "),
                _c("ul", [
                  _c("li", [
                    _vm._v(
                      "\n                LAYANAN DAN KONTEN DARI SITUS INI DISEDIAKAN “SEPERTI\n                SEDIAKALA” DAN “SEBAGAIMANA ADA”. KAMI DENGAN TEGAS MENYATAKAN\n                BAHWA KAMI TIDAK MENYEDIAKAN JAMINAN ATAU GARANSI BAIK SECARA\n                TEGAS ATAUPUN TERSIRAT SEHUBUNGAN DENGAN BERHASIL ATAU\n                TIDAKNYA SUATU PENGAJUAN ATAU KESIAPAN DARI SUATU PRODUK YANG\n                DISEDIAKAN OLEH PIHAK KETIGA;\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [_vm._v("KAMI TIDAK MENYEDIAKAN JAMINAN BAHWA")]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                FUNGSI DAN JASA YANG DISEDIAKAN OLEH SITUS AKAN BEBAS DARI\n                GANGGUAN ATAU KESALAHAN TERMASUK NAMUN TIDAK TERBATAS PADA\n                KEAMANAN SITUS INI;\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v("KELALAIAN AKAN DIPERBAIKI; ATAU")
                  ]),
                  _vm._v(" "),
                  _c("li", { staticClass: "ml-4" }, [
                    _vm._v(
                      "\n                SITUS INI ATAU SERVER YANG MENYEDIAKAN SITUS INI AKAN BEBAS\n                DARI VIRUS ATAU KOMPONEN MALWARE;\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                KAMI TIDAK MEMBERIKAN GARANSI TENTANG AKURASI, KEASLIAN,\n                INTEGRITAS ATAU KUALITAS KONTEN, SITUS ATAU SUMBER ATAU TAUTAN\n                YANG ADA PADA SITUS, TERMASUK NAMUN TIDAK TERBATAS PADA\n                JAMINAN KONTEN, SITUS ATAU SUMBER YANG MUNGKIN MENGANDUNG\n                MATERI YANG MENGGANGGU, TIDAK ETIS ATAU MATERI YANG BELUM\n                PASTI;\n              "
                    )
                  ]),
                  _vm._v(" "),
                  _c("li", [
                    _vm._v(
                      "\n                SETIAP DAN SELURUH PRODUK YANG DIUNDUH ATAU DIDAPATKAN DARI\n                SITUS MERUPAKAN TANGGUNG JAWAB DAN RISIKO PRIBADI ANDA. ANDA\n                WAJIB BERTANGGUNGJAWAB SECARA PENUH ATAS KERUSAKAN SISTEM ATAU\n                KEHILANGAN DATA YANG DIAKIBATKAN OLEH PENGUNDUHAN MATERI\n                TERSEBUT.\n              "
                    )
                  ])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Pembatasan Tanggung Jawab")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Dalam hal terdapat sengketa dan/atau konflik, maka Kami berhak\n              namun tidak berkewajiban untuk memediasi dan memfasilitasi\n              persengketaan tersebut dan Anda dengan ini sepakat untuk\n              memberikan indemnifikasi kepada Kami dan/atau membebaskan Kami\n              dari segala jenis tuntutan, kehilangan atau kerugian yang\n              mungkin Anda derita akibat sengketa dan/atau konflik tersebut\n              (dengan atau tanpa peran serta kami dalam memediasi/\n              memfasilitasi Anda).\n            "
                  )
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Anda dengan ini menerima dan menyetujui bahwa Kami bukan\n              merupakan pihak penyedia jasa kepada Anda, bukan merupakan\n              institusi keuangan, penyedia asuransi atau penasehat keuangan.\n              Layanan Kami hanya terbatas pada menyediakan informasi yang\n              diberikan oleh penyedia jasa Pihak Ketiga untuk menjembatani\n              kebutuhan Anda dengan penyedia jasa pilihan Anda tersebut. Kami\n              tidak membuat keputusan mengenai cakupan atau kredit dengan\n              penyedia jasa yang Anda pilih. Kami tidak menyediakan produk\n              keuangan secara langsung seperti penggadaian, pinjamaan,\n              asuransi dan lain sebagainya.\n            "
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Pengabaian")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Kegagalan pelaksanaan atau keterlambatan dalam melaksanakan hak,\n              kuasa atau hak khusus Kami yang ada pada Syarat Penggunaan\n              dan/atau Perjanjian Layanan tidak dianggap sebagai pengabaian\n              dari hak tersebut; atau pelaksanaan satu atau sebagian dari hak,\n              kuasa atau hak khusus berdasarkan Syarat Penggunaan dan/atau\n              Perjanjian Layanan tidak dapat menghalangi pelaksanaan lain atau\n              pelaksanaan lanjutan dari hak, kuasa atau hak khusus tersebut.\n              Hak dan perbaikan yang disediakan disini bersifat kumulatif dan\n              tidak ekslusif dari yang disediakan oleh Hukum.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Anda setuju bahwa pada kejadian pemutusan, sejauh pemutusan\n              tersebut membutuhkan keputusan pengadilan maka Anda dengan ini\n              menyatakan setuju untuk mengabaikan Pasal 1266 Kitab\n              Undang-Undang hukum Perdata hanya sejauh putusan pengadilan\n              diperlukan untuk memutuskan atau membatalkan Syarat Penggunaan\n              atau Perjanjian Layanan.\n            "
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Keadaan Memaksa")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Kami tidak dapat dimintai pertanggungjawaban atas keterlambatan\n              atau kegagalan dalam pelaksanaan Layanan berdasarkan Syarat\n              Penggunaan ini sejauh keadaan tersebut diakibatkan oleh suatu\n              Keadaan Memaksa.\n            "
                  )
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col text-left" }, [
                _c("h1", [_vm._v("Situs ini- FAQ")]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Tiap Layanan disediakan oleh Kami, tim Uemka yang berkolaborasi\n              untuk menyediakan jasa terbaik bagi Anda.\n            "
                  )
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v(
                    "\n              Apabila terdapat pertanyaan harap merujuk pada pertanyaan di\n              FAQs pada Situs web\n            "
                  )
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/TermConditions.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/TermConditions.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TermConditions.vue?vue&type=template&id=e1ecfdba& */ "./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/TermConditions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./TermConditions.vue?vue&type=template&id=e1ecfdba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/TermConditions.vue?vue&type=template&id=e1ecfdba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TermConditions_vue_vue_type_template_id_e1ecfdba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);