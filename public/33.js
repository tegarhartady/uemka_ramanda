(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("main", { attrs: { id: "main" } }, [
      _c("section", { staticClass: "blog", attrs: { id: "blog" } }, [
        _c("div", { staticClass: "container" }, [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-lg" }, [
              _c("h3", [_vm._v("Daftar Sebagai Lembaga")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("p", [_vm._v("Informasi Pinjaman")])
            ])
          ]),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("form", [
                _c("label", [_vm._v("Jumlah Pinjaman")]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group mb-3" }, [
                  _c("div", { staticClass: "input-group-prepend" }, [
                    _c(
                      "span",
                      {
                        staticClass: "input-group-text",
                        attrs: { id: "basic-addon1" }
                      },
                      [_vm._v("Rp")]
                    )
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      placeholder: "",
                      "aria-label": "Username",
                      "aria-describedby": "basic-addon1"
                    }
                  })
                ]),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticClass: "ml-4",
                    staticStyle: { "font-size": "14px !important" }
                  },
                  [
                    _vm._v(
                      "Silakan masukkan jumlah pinjaman maksimal Rp 30.000.000"
                    )
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "form-group" }, [
                  _c("label", { attrs: { for: "exampleInputEmail1" } }, [
                    _vm._v("Jangka Waktu")
                  ]),
                  _vm._v(" "),
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "exampleInputEmail1",
                      "aria-describedby": "emailHelp",
                      placeholder: ""
                    }
                  })
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row mt-5" }, [
            _c("div", { staticClass: "col-lg" }, [
              _c("h5", [_vm._v("Informasi Domisili")]),
              _vm._v(" "),
              _c("p", { staticClass: "lead mt-5" }, [_vm._v("Provinsi")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("form", [
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("DKI Jakarta")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Jawa Barat")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Jawa Timur")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Jawa Tengah")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Sumatera Utara")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Lainnya")]
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row mt-5" }, [
            _c("div", { staticClass: "col-lg" }, [
              _c("h5", [_vm._v("Informasi Pekerjaan")]),
              _vm._v(" "),
              _c("p", { staticClass: "lead mt-5" }, [_vm._v("Pekerjaan")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col" }, [
              _c("form", [
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Karyawan")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Wiraswasta")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Profesional")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Ibu Rumah Tangga")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Pegawai Negeri Sipil")]
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "form-group form-check",
                    staticStyle: { border: "1px solid", height: "50px" }
                  },
                  [
                    _c("input", {
                      staticClass: "form-check-input ml-2 mt-3",
                      attrs: { type: "checkbox", id: "exampleCheck1" }
                    }),
                    _vm._v(" "),
                    _c(
                      "label",
                      {
                        staticClass: "form-check-label ml-5",
                        staticStyle: { "margin-top": "10px !important" },
                        attrs: { for: "exampleCheck1" }
                      },
                      [_vm._v("Lainnya")]
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "text-right" }, [
            _c("button", { staticClass: "btn btn-warning" }, [
              _vm._v("Selanjutnya")
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Paket_pinjol.vue":
/*!*********************************************!*\
  !*** ./resources/js/views/Paket_pinjol.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Paket_pinjol.vue?vue&type=template&id=03f904e7& */ "./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Paket_pinjol.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Paket_pinjol.vue?vue&type=template&id=03f904e7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paket_pinjol.vue?vue&type=template&id=03f904e7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paket_pinjol_vue_vue_type_template_id_03f904e7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);