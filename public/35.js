(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[35],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("main", { attrs: { id: "main" } }, [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col mt-5" }, [
            _c("h1", [_vm._v("Profile Anda")]),
            _vm._v(" "),
            _c("p", [_vm._v("Selamat datang,Tegar")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row text-center" }, [
          _c("div", { staticClass: "col  Profile" }, [
            _c("img", {
              staticClass: "rounded-circle",
              attrs: { src: "/assets/img/lilo.png" }
            })
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center mt-5" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/user.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Nama lengkap")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [
                    _vm._v("Lilo Digdya Pasha Santoso")
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/telp.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Nomor telepon")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [
                    _vm._v("0821 2312-2312")
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/mail.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Email")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [
                    _vm._v("lilopsh@yahoo.com")
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/instagram.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Social media")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [_vm._v("@lilopasha")])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/jobs.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Bidang pekerjaan")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [
                    _vm._v("Pengusaha otomotif")
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row justify-content-center" }, [
          _c("div", { staticClass: "col-sm-6" }, [
            _c("div", { staticClass: "media" }, [
              _c("img", {
                staticClass: "mr-3",
                attrs: { src: "/assets/img/location.png", alt: "" }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "media-body" }, [
                _c("div", { staticClass: "testimonial" }, [
                  _c("p", [_vm._v("Alamat domisili")]),
                  _vm._v(" "),
                  _c("p", { staticClass: "overview" }, [
                    _vm._v(
                      "Sunburst CBD, Jl. Kapten Soebijanto Djojohadikusumo \n                                Jl. Komp. Bsd No.2, Lengkong Gudang, City, Kota Tangerang Selatan, Banten 15322"
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row text-center info-guys" }, [
        _c("div", { staticClass: "col" }, [
          _c("h4", [
            _vm._v("Terus gunakan layanan infomodal untuk meningkatkan "),
            _c("br"),
            _vm._v(" level customer card Anda dan dapatkan keuntungannya")
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "row text-center kartumember" }, [
        _c("div", { staticClass: "col" }, [
          _c("img", { attrs: { src: "/assets/img/kartumember.png", alt: "" } }),
          _vm._v(" "),
          _c("div", [
            _c("button", { staticClass: "btn btn-warning" }, [
              _vm._v("Ajukan Peningkatan Level")
            ])
          ]),
          _vm._v(" "),
          _c("p", [_vm._v("Keluar Akun")])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Updateprofile.vue":
/*!**********************************************!*\
  !*** ./resources/js/views/Updateprofile.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Updateprofile.vue?vue&type=template&id=6ee52b9e& */ "./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Updateprofile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e&":
/*!*****************************************************************************!*\
  !*** ./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Updateprofile.vue?vue&type=template&id=6ee52b9e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Updateprofile.vue?vue&type=template&id=6ee52b9e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Updateprofile_vue_vue_type_template_id_6ee52b9e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);