import './bootstrap'
import Vue from 'vue'
import router from './router'
import App from './layouts/App.vue'
import firebase from 'firebase'
// import Vuesession from 'vue-session'
import VueSplide from '@splidejs/vue-splide';

Vue.use( VueSplide );

Vue.prototype.$env = process.env.VUE_APP_URL

import Vuesession from 'vue-session'
// @ts-ignore
Vue.use(Vuesession)




firebase.initializeApp({
    apiKey: "AIzaSyCMPqOx390IIibfgJ-5BH4jiG112-d8pSg",
    authDomain: "uemka-ce090.firebaseapp.com",
    projectId: "uemka-ce090",
    storageBucket: "uemka-ce090.appspot.com",
    messagingSenderId: "985643452979",
    appId: "1:985643452979:web:62bd4c3977475e56ac3146",
    measurementId: "G-DKNNGGBLNR"
})

import VueGoodTablePlugin from 'vue-good-table';
Vue.use(VueGoodTablePlugin);
import 'vue-good-table/dist/vue-good-table.css'

import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

Vue.use(VueMoment, {moment,})

import CKEditor from '@ckeditor/ckeditor5-vue2';
Vue.use( CKEditor );

import VueSession from 'vue-session'
Vue.use(VueSession);

import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading, {
    // props
    color: '#FF7F00',
    height: 70,
    zIndex: 999999999
  }, {
    // slots
  })

import Vuesweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

// const app = createApp(App)

Vue.use(Vuesweetalert2);

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
new Vue({
    router,
    el: '#app',
    render: h => h(App)
})
