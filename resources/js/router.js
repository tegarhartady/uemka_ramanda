import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: "history",
    routes: [{
        path: '',
        component:()=>import('./layouts/Main.vue'),
        children: [
        {
        path: '/',
        name: 'Home',
        component: () =>
        import("./views/Home.vue")
        },{
            path: '/artikel',
            name: 'artikel',
            component: () =>
                import("./views/Artikel.vue")
    
        },{
        path: '/Success',
        name: 'succes',
        component: () =>
            import("./views/Sukses.vue")

        },{
        path: '/updateprofile',
        name: 'updateprofile',
        component: () =>
            import("./views/Updateprofile.vue")
        },{
        path: '/kspperusahaan/:id',
        name: 'kspperusahaan',
        component: () =>
            import("./views/kspperusahaan.vue")
        },{
        path: '/Paket',
        name: 'Paket',
        component: () =>
            import("./views/Paket.vue")
        },{
        path: '/Paketpinjol',
        name: 'Paket_pinjol',
        component: () =>
            import("./views/Paket_pinjol.vue")
        },{
        path: '/Blog',
        name: 'Blog',
        component: () =>
            import("./views/Blog.vue")
        },{
        path: '/1Login',
        name: 'Login',
        component: () =>
        import("./views/login/Login.vue")
        },{
         path: '/Promosi/:slug',
         name: 'promosi',
         component: () =>
        import("./views/Promosi.vue")
        },{
            path: '/Promosipage',
         name: 'promosi',
         component: () =>
        import("./views/Promosipage.vue")
                },{
         path: '/Detailkspdankpr',
         name: 'promosi',
         component: () =>
        import("./views/bprdanksp.vue")
         },
         {
         path: '/TermsConditions',
         name: 'promosi',
         component: () =>
        import("./views/TermConditions.vue")

                },{
         path: '/Privacy',
         name: 'promosi',
         component: () =>
         import("./views/TermConditions.vue")

            },
            {
                path: '/Faq',
                name: 'promosi',
                component: () =>
                import("./views/Faq.vue")
       
                   },{
            path: '/Pageartikel/:slug',
            name: 'promosi',
             component: () =>
              import("./views/Artikelpage.vue")

}]

},
{
    path: '/',
        component: () => import('./layouts/auth/App.vue'),
        children : [{
            path: 'masuk',
            name: 'login',
            component: () =>
                import("./views/login/Login.vue")
        
            },
            {
                path: 'daftar',
                name: 'daftar',
                component: () =>
                    import("./views/login/Register.vue")
            
            }
        ]
    },
    {
        path: '',
            component: () => import('./layouts/template/App.vue'),
            children : [{
                path: '/Partner',
                name: '',
                component: () =>
                    import("./views/cms/Partner.vue")
            
                },
                {
                    path: '/Uploadartikel',
                    name: '',
                    component: () =>
                        import("./views/cms/Addartikel.vue")
                },{
                    path: '/Uploadpromosi',
                    name: '',
                    component: () =>
                        import("./views/cms/Addpromosi.vue")
                },{
                    path: '/Uploadtestimonial',
                    name: '',
                    component: () =>
                        import("./views/cms/Addtestimonial.vue")
                },
                {
                    path: '/Addpartner',
                    name: '',
                    component: () =>
                        import("./views/cms/Addpartner.vue")
                },{
                    path: '/Produk',
                    name: '',
                    component: () =>
                        import("./views/cms/Produk.vue")
                },{
                    path: '/Dashboard',
                    name: '',
                    component: () =>
                        import("./views/cms/Dashboard.vue")
                },{
                    path: '/Payment',
                    name: '',
                    component: () =>
                        import("./views/cms/Payment.vue")
                },{
                    path: '/applog',
                    name: '',
                    component: () =>
                        import("./views/cms/Aplicationlog.vue")
                },{
                    path: '/Listpartner',
                    name: '',
                    component: () =>
                        import("./views/cms/Listpartner.vue")
                },
                {
                    path: '/Listproduk',
                    name: '',
                    component: () =>
                        import("./views/cms/Listproduk.vue")
                },{
                      
                    path: '/Addpartner',
                    name: '',
                    component: () =>
                        import("./views/cms/Addpartner.vue")
                
                },{
                      
                    path: '/Addproduk',
                    name: '',
                    component: () =>
                        import("./views/cms/Addproduk.vue")
                
                },{
                      
                    path: '/Listartikel',
                    name: '',
                    component: () =>
                        import("./views/cms/Listartikel.vue")
                
                },{
                      
                    path: '/Listpromosi',
                    name: '',
                    component: () =>
                        import("./views/cms/Listpromosi.vue")
                
                },{
                      
                    path: '/Listtestimonial',
                    name: '',
                    component: () =>
                        import("./views/cms/Listtestimonial.vue")
                
                },{
                      
                    path: '/profilecms',
                    name: '',
                    component: () =>
                        import("./views/cms/profile.vue")
                
                }


            ]
        },
    
    

]


    
});



export default router