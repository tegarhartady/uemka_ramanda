<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Uemka</title>
    <link rel="icon" href="/assets/img/logo/logouemka.png" type="image/icon type">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    {{-- <link href="/assets/img/favicon.png" rel="icon"> --}}
    <link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  
    <!-- Google Fonts -->

    <link
  rel="stylesheet"
  href="https://unpkg.com/img-comparison-slider@3/dist/collection/styles/initial.css"
/>
  <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
      href="https://fonts.googleapis.com/css2?family=Manrope:wght@200&display=swap"
      rel="stylesheet">
  
    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="/assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="/assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/assets/vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
      integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <!-- Template Main CSS File -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/sbadmin/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>
    <div id="app"></div>
    <script src="{{mix('js/app.js')}}">
    </script>
    
  <!-- Vendor JS Files -->
  <script src="/assets/vendor/jquery/jquery.min.js"></script>
  <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="/assets/vendor/php-email-form/validate.js"></script>
  <script src="/assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="/assets/vendor/venobox/venobox.min.js"></script>
  <script src="/assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="/assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="/assets/vendor/aos/aos.js"></script>
  <script src="/sbadmin/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="/sbadmin/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.js"></script>
  <script src="https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js"></script>
  <script src="vuelidate/dist/vuelidate.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer>
  </script> 
  <script src="/path/to/vue.js"></script>
  <script src="/path/to/vue-carousel.js"></script>
  <!-- Template Main JS File -->
  <script src="/assets/js/main.js"></script>
</body>
</html>