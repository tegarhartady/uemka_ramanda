<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    // Login
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::get('viewall','AuthController@view_user');

    // lembaga
    Route::post('lembaga/register','LembagaController@signup');
    Route::get('lembaga', 'LembagaController@index');
    Route::post('lembaga/edit/{id_perusahaan}', 'LembagaController@edit');
    Route::post('lembaga/hapus/{id_perusahaan}', 'LembagaController@destroy');
    Route::get('lembaga/viewall/{id_perusahaan}', 'LembagaController@viewuser');


    // Parnter
    Route::post('partner/tambah', 'PartnerController@tambah_partner');
    Route::put('partner/{idpartner}', 'PartnerController@update_partner');
    Route::get('partner', 'PartnerController@index');
    Route::get('partner/hapus/{idpartner}','PartnerController@destroy');
    Route::get('partner/jenis/kospin','PartnerController@show_aspartnerkospin');
    Route::get('partner/jenis/bpr','PartnerController@show_aspartnerbpr');
    Route::put('partner/verifikasi/{idpartner}','AuthController@verifikasipartner');
    Route::post('partner/ambildata/kospin', 'PartnerController@whereidkospin');
    Route::get('partner/jenis/all/kospin','PartnerController@showall_aspartnerkospin');
    Route::get('partner/jenis/all/bpr','PartnerController@showall_aspartnerbpr');

    // Article
    Route::post('article','ArtikelController@index');
    Route::post('article/tambah','ArtikelController@tambah_artikel');
    Route::put('article/{idarticle}', 'ArtikelController@update_artikel');
    Route::get('article/full/{idarticle}','ArtikelController@fullarticle');
    Route::get('article/hapus/{idarticle}','ArtikelController@destroy');
    Route::get('article/ambildata','ArtikelController@whereslug');
    // Product
    Route::post('product/tambah', 'ProductController@tambah_product');
    Route::put('product/{idproduct}', 'ProductController@update_product');
    Route::get('product', 'ProductController@index');
    Route::get('product/hapus/{idproduct}','ProductController@destroy');
    Route::get('product/partner/{idpartner}','ProductController@productpartner');
    Route::get('product/partner/limit/{idpartner}','ProductController@productpartnerrr');
    Route::get('product/rate/{idproduct}','ProductController@rate');

    // Promosi
    Route::post('promosi/tambah', 'PromosiController@addpromosi');
    Route::put('promosi/{idpromosi}', 'PromosiController@update_promosi');
    Route::get('promosi', 'PromosiController@index');
    Route::get('promosi/hapus/{idpromosi}','PromosiController@destroy');
    Route::post('promosi/ambildata', 'PromosiController@whereslug');
    Route::post('promosi/forslider', 'PromosiController@perlimit');

    // Testimoni
    Route::get('testimoni', 'TestiController@index');
    Route::post('testimoni/tambah', 'TestiController@add_testi');
    Route::post('testimoni/edit/{idtesti}', 'TestiController@edit_testi');
    Route::get('testimoni/hapus/{idtesti}', 'TestiController@hapus');
    Route::get('testimoni/limit', 'TestiController@perlimit');


    // Route::post('testsa', 'TestiController@add_testi');

    // Kalkulator
    Route::get('kalkulator','KalkulatorController@index');
    Route::post('Kalkulator/tambah','KalkulatorController@tambah');
    Route::put('kalkulator/edit/{idkalkulator}','KalkulatorController@edit');
    Route::get('kalkulator/hapus/{idkalkulator}','KalkulatorController@hapus');
    Route::post('kalkulator/hitung','KalkulatorController@menghitung');
    

    // Submit Application
    Route::post('submitkalkulator/{idkalkulator}','SubmitController@savefromkalku');
    Route::put('editsubmitkalkulator/{idkalkulator}','SubmitController@editformkalku');
    Route::get('hapussubmitkalkulator/{idkalkulator}','SubmitController@hapus');
    Route::get('tampilsubmit','SubmitController@index');

    // Jangka Waktu Pinjaman
    Route::get('wapin','JangkaWapinController@index');
    Route::post('wapin/tambah','JangkaWapinController@tambahwapin');
    Route::post('wapin/edit/{id_wapin}','JangkaWapinController@edit');
    Route::get('wapin/hapus/{id_wapin}','JangkaWapinController@hapus');
    // Test
    // Route::post('testss','TestController@add_test');
    

    // report for all
    Route::get('sumkospin','Report\ReportSummaryController@sumkospin');    
    Route::get('sumbpr','Report\ReportSummaryController@sumbpr');
    Route::get('terdaftar','Report\ReportSummaryController@terdaftar');    
    Route::get('getall','Report\ReportSummaryController@getalldata');    
    Route::get('listpartner','Report\ListofController@listpartner');

    // Summary Billing Payment
    Route::get('paymentgetall', 'Report\PaymentBillingController@getall');    
    // Reset Password
    Route::post('password/create', 'PasswordResetController@create');
    Route::get('password/find/{token}', 'PasswordResetController@find');
    // Route::post('password/reset', 'PasswordResetController@reset');
    // Route::post('password', 'PasswordResetController@test');

    Route::post('changepassword', 'AuthController@changePassword');

    Route::post('password-reset', 'AuthController@sendPasswordResetToken');
    // Route::get('reset-password/{token}', 'PasswordController@showPasswordResetForm');
    Route::post('reset-password/{token}', 'PasswordController@resetPassword');
        // Forgot Password
    Route::post('sendotpemail', 'AuthController@sendOtpEmail');
    Route::post('verificationcode', 'AuthController@verificationCode');
    Route::post('changeforgotpassword', 'AuthController@changeForgotPassword');
    Route::post('emailcheck', 'PasswordResetController@emailCheck');


    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        // Route::put('update/{id}', 'AuthController@update');
    });
});

Route::group([
    'prefix' => 'lembaga'
], function () {
    
});

Route::group([    
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    // Route::post('create', 'PasswordResetController@create');
    // Route::get('find/{token}', 'PasswordResetController@find');
    // Route::post('reset', 'PasswordResetController@reset');
});